include Edn

type t = [%import: Edn.t]
[@@deriving sexp, bin_io, hash, compare, equal, show]

let of_symbol (s : string) : t = `String s
let of_int (i : int) :t = `Int i
let min_value : t = `Null
let of_time time = `Int time
let of_string s = `String s
let symbol_name = function
                | `String name -> name
                | _ -> failwith "Expected symbol"

let cardinal_one = `String "one"
let cardinal_many = `String "many"

module Types = struct
    type value = t
    [@@deriving sexp, bin_io, hash, compare, equal, show]

    type t = value
    [@@deriving sexp, bin_io, hash, compare, equal, show]

    let db_symbol = `String "db/symbol"
    let db_type = `String "db/type"
    let db_cardinality = `String "db/cardinality"
    let db_timestamp =  `String "db/timestamp"
    let db_string = `String "db/string"
    let db_int = `String "db/int"
    let db_user = `String "db/user"
    let db_ref = `String "db/ref"

    let is_type t =
        match t with
        | `String "db/symbol"
        | `String "db/type"
        | `String "db/cardinality"
        | `String "db/timestamp"
        | `String "db/string"
        | `String "db/int"
        | `String "db/user"
        | `String "db/ref" -> true
        | _ -> false
    ;;

    let is_valid (t : t) (v : value) : bool =
        (*Printf.printf "%s => %s\n" (show_value t) (show_value v);*)
        match t, v with
        | `String "db/symbol", `Symbol _ -> true
        | `String "db/timestamp", `Int _ -> true
        | `String "db/string", `String _ -> true
        | `String "db/int", `Int _ -> true
        | `String "db/user", _ -> true
        | `String "db/ref", `Int _ -> true
        | `String "db/cardinality", `String "one" -> true
        | `String "db/cardinality", `String "many" -> true
        | `String "db/symbol", `String _ -> true
        | `String "db/type", v  when is_type v -> true
        | _ -> false
    ;;

    let is_symbol = function
        | `Symbol _ -> true
        | _ -> false

        (* TODO *)
    let is_timestamp = function
        | `Int _ -> true
        | _ -> false

    let is_user _ = true

    let to_value t = t
    let of_value t = t
end
