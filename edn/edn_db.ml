open Ocaml_datalog

include Db.Make(Edn_sig)

module Value = Edn_sig

type func_map = Func.t String.Map.t
let empty_funcs = String.Map.empty

let query_from_edn ?(funcs=empty_funcs) t = 
    QueryEdn.query_from_edn ~funcs (find_attribute_id t)
;;

let query_from_string ?(funcs=empty_funcs) t str =
    str
    |> Edn.from_string
    |> QueryEdn.query_from_edn ~funcs (find_attribute_id t)
;;
