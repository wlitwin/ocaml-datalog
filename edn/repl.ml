module Db = Edn_db

let prompt () =
    Caml.print_string "> "
;;

let error str =
    Caml.print_endline ("Error " ^ str);
;;

let with_exn fn =
    try 
        fn ()
    with e ->
        error Exn.(to_string e)
;;

let do_query str db =
    with_exn (fun () ->
        match Db.query_from_string [db] str with
        | Error err -> error err
        | Ok (query, rules, _) ->
            let res = Db.query [db] query rules in
            Caml.print_endline Db.(string_of_result res)
    );
    db
;;

let build_datom (id : int) (attr : string) (value : Db.Txn.value) =
    Db.(Txn.(
        let id = eid_of_int id in
        datom empty id attr value
    ))
;;

let parse_value value =
    if String.is_prefix ~prefix:"`" value then (
        let value = String.sub ~pos:1 ~len:(String.length value - 1) value in
        Db.Txn.(Ref (Int.of_string value |> eid_of_int))
    ) else (
        Db.Txn.Val Edn.(from_string value)
    )

let do_add id attr value db =
    let id = Int.of_string id in
    let value = parse_value value in
    let datom = build_datom id attr value in
    Db.add db datom
;;

let do_delete id attr value db =
    let id = Int.of_string id in
    let value = parse_value value in
    let datom = build_datom id attr value in
    Db.delete db datom
;;

let parse_line input state =
    let cmds = String.split ~on:' ' input in
    match cmds with
    | "print" :: _ -> Caml.print_endline Db.(to_string state); state
    | "query" :: query -> do_query (String.concat ~sep:" " query) state
    | "add" :: id :: attr :: [value] -> do_add id attr value state
    | "add" :: _ -> error "add has the form 'add id attr value'"; state
    | "del" :: id :: attr :: [value] -> do_delete id attr value state
    | "del" :: _ -> error "del has the form 'add id attr value'"; state
    | _ -> error "Unrecognized command"; state
;;

let repl () =
    let rec loop state =
        prompt ();
        try
            let input = Caml.read_line () in
            loop (parse_line input state)
        with End_of_file -> Caml.print_endline ""
    in
    loop Db.empty
;;
