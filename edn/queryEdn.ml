open Ocaml_datalog

module Atom = Atom.Make(Edn_sig)
module Term = Term.Make(Edn_sig)
module Rule = Rule.Make(Edn_sig)

exception Empty_pattern
exception Pattern_too_large
exception Expected_rule_name
exception Expected_rule_body
exception Expected_rule_head

let query_from_edn ?(funcs=String.Map.empty) (lookup : string -> int) (edn : Edn_sig.t) : ((Atom.t * Rule.Func.map) * Rule.t list * string list, string) Result.t =
    let vars = ref StringIntern.empty in
    let next_id = ref 0 in
    let intern str = 
        let id, strs = StringIntern.intern_fn !vars str ~f:(fun () ->
            let id = !next_id in
            incr next_id;
            id
        ) in
        vars := strs;
        id
    in

    let func_name_map, func_map =
        let id = ref ~-1 in
        String.Map.fold ~init:(String.Map.empty, Int.Map.empty) ~f:(fun ~key ~data (name_map, func_map) ->
            incr id;
            (String.Map.add_exn name_map ~key ~data:!id,
            Int.Map.add_exn func_map ~key:!id ~data)
        ) funcs
    in

    (* Find all variables and assign unique IDs *)

    let to_pat = function
        | `Symbol (None, "_") -> `Ignore
        | `Symbol (None, var) -> `Var (intern var)
        | `Symbol (Some namespace, var) -> `Var (intern (namespace ^ "/" ^ var))
        | `Keyword (None, attr) -> `Int (lookup attr)
        | `Keyword (Some namespace, attr) -> `Int (lookup (namespace ^ "/" ^ attr))
        | `Tag (None, "ref", `Int n) -> `Int n
        (*| `Int i -> `Int i*)
        | v -> `Val v
    in

    let to_term : Edn.t -> Term.t = function
        | `Symbol (None, var) -> `Var (intern var)
        | `Symbol (Some namespace, var) -> `Var (intern (namespace ^ "/" ^ var))
        | `Keyword (None, attr) -> `Int (lookup attr)
        | `Keyword (Some namespace, attr) -> `Int (lookup (namespace ^ "/" ^ attr))
        | `Tag (None, "ref", `Int n) -> `Int n
     (*   | `Int i -> `Int i*)
        | v -> `Val v
    in

    let to_atom_body : Edn.t list -> Rule.Clause.t = function   
        | [] -> raise Empty_pattern
        | [p1] -> Atom (to_pat p1, `Ignore, `Ignore, `Ignore)
        | [p1;p2] -> Atom (to_pat p1, to_pat p2, `Ignore, `Ignore)
        | [p1;p2;p3] -> Atom (to_pat p1, to_pat p2, to_pat p3, `Ignore)
        | [p1;p2;p3;p4] -> Atom (to_pat p1, to_pat p2, to_pat p3, to_pat p4)
        | _ -> raise Pattern_too_large
    in

    let cond_body op e1 e2 =
        Rule.Clause.Cond (op, to_term e1, to_term e2)
    in

    let module C = Rule.Cond in

    let to_rule_ref = function
        | `Symbol (None, ">") :: e1 :: [e2] -> cond_body C.Gt e1 e2
        | `Symbol (None, ">=") :: e1 :: [e2] -> cond_body C.Gte e1 e2
        | `Symbol (None, "<") :: e1 :: [e2] -> cond_body C.Lt e1 e2
        | `Symbol (None, "<=") :: e1 :: [e2] -> cond_body C.Lte e1 e2
        | `Symbol (None, "=") :: e1 :: [e2] -> cond_body C.Eq e1 e2
        | `Symbol (None, "!=") :: e1 :: [e2] -> cond_body C.Neq e1 e2
        | `Symbol (None, name) :: args -> 
            let args = List.map ~f:to_pat args in
            Rule.Clause.(RuleRef (name, args))
        | _ -> raise Expected_rule_name
    in

    let validate_fun_args args =
        List.map args ~f:(function
            | `Symbol (None, var) -> `Var (intern var)
            | v -> `Val v
        )
    in

    let validate_fun_outs outs =
        Printf.printf "Outputs size: %d\n" List.(length outs);
        List.map outs ~f:(function
            | `Symbol (None, var) -> 
                    let o = intern var in
                    Printf.printf "Output %s -> %d\n" var o;
                    o
            | _ -> failwith "Function outs can only be variables"
        )
    in

    let to_func name args outs =
        Caml.print_endline ("function " ^ name);
        let id = String.Map.find_exn func_name_map name in
        Rule.Clause.Func (id, validate_fun_args args, validate_fun_outs outs)
    in

    let to_rule_body = function
        | `List ((`Symbol (None, "not")) :: [`List ruleref]) -> Rule.Not, (to_rule_ref ruleref)
        | `List ((`Symbol (None, "not")) :: [`Vector atom]) -> Rule.Not, (to_atom_body atom)
        | `List ruleref -> Rule.Pos, to_rule_ref ruleref 
        | `Vector (`List ((`Tag (None, "fun", `Symbol (None, name))) :: args) :: outs) ->
            Rule.Pos, to_func name args outs
        | `Vector atom -> Pos, (to_atom_body atom)
        | _ -> raise Expected_rule_body
    in

    let collect_query lst =
        let rec loop acc = function
            | [] -> acc, []
            | hd :: rest ->
                try loop ((to_rule_body hd) :: acc) rest
                with Expected_rule_body -> acc, (hd :: rest)
        in
        loop [] lst
    in

    let to_rule_head = function
        | `Symbol (None, name) :: args ->
            let args = List.map ~f:to_term args in
            (name, args)
        | _ -> raise Expected_rule_head
    in

    let to_rule : Edn.t list -> Rule.t = function
        | `List head :: body -> 
            let rule_head = to_rule_head head in
            let body = List.map ~f:to_rule_body body in
            (rule_head, body)
        | _ -> raise Expected_rule_head
    in

    let collect_rules : Edn.t list -> Rule.t list = function
        | `Keyword (None, "rules") :: rest ->
            let rec loop acc = function
                | (`Vector rule) :: rest -> loop (to_rule rule :: acc) rest
                | _ -> acc
            in
            loop [] rest
        | _ -> []
    in

    let unique_rule_name rule_set =
        let rec loop name =
            if String.Set.mem rule_set name then (  
                let num = Random.int 1000000 in
                loop ("query_rule" ^ Int.to_string num)
            ) else name
        in
        loop "query_rule"
    in

    let get_vars vars =
        let alist = StringIntern.StringTree.to_alist vars in
        let sorted = List.sort alist ~compare:(fun (_, id1) (_, id2) -> Int.compare id1 id2) in
        List.map sorted ~f:fst
    in

    let build_query rule_set rvars body rules =
        match body with
        | Rule.[Pos, Clause.RuleRef (name, args)] when Rule.Pattern.all_terms args ->
            (* Simple optimization, if the query body is a single rule reference
             * use that as the query instead of promoting to a new indirect rule 
             *)
            ((name, Rule.Pattern.cast_terms args), func_map), rules, get_vars !vars
        | _ ->
            let query_name = unique_rule_name rule_set in
            let query_rule : Rule.t = (query_name, rvars), List.rev body in
            let rules = query_rule :: rules in
            ((query_name, rvars), func_map), rules, get_vars !vars
    in

    match edn with
    | `Vector (
        (`Keyword (None, "find")) ::
        (`Vector query_vars) :: (`Keyword (None, "where")) :: rest)
        -> 
            let query_vars = List.map ~f:to_term query_vars in
            let query_body, rest = collect_query rest in
            let rules = collect_rules rest in
            let rule_set = Rule.rule_name_set rules in
            Ok (build_query rule_set query_vars query_body rules)

    | _ -> Error "Bad query"
;;

