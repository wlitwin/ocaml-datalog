module Make(Value : Sigs.Value) = struct
    module Atom = Atom.Make(Value)
    module Term = Term.Make(Value)
    module TermList = Term.ConcreteTermList
    module Entity = Entity.Make(Value)
    module Rule = Rule.Make(Value)
    module Sub = Sub.Make(Value)

    let substitute_vars ((head, terms) : Atom.t) (subs : Sub.t) : Atom.t =
        let terms = List.map terms ~f:(function
            | `Int _ as i -> i
            | `Val _ as v -> v
            | `Var x ->
                match Sub.lookup subs x with
                | None -> `Var x
                | Some (`Int i) -> `Int i
                | Some (`Val v) -> `Val v
        ) in
        (head, terms)
    ;;

    let unify_terms (clause_body : Term.t list) (fact_body : TermList.t) = 
        (* Fact is ground *)
       let rec loop subs = function
           | [] -> Some subs
           | (`Val v, `Val y) :: rest when Value.(equal v y) -> loop subs rest
           | (`Int i, `Int y) :: rest when i = y -> loop subs rest
           | (_, `Val _) :: _
           | (_, `Int _) :: _ -> None
           | ((`Int _ as s), `Var v) :: rest
           | ((`Val _ as s), `Var v) :: rest ->
                match Sub.lookup subs v with
                | None -> loop (Sub.extend subs v s) rest
                | Some l when Sub.(equal_value s l) -> loop subs rest
                | Some _ -> None
       in
       let res = loop Sub.empty List.(zip_exn fact_body clause_body) in
       (*Printf.printf "Unifying %s with %s res = %s\n" 
        (string_of_atom (clause_head, clause_body)) 
        (string_of_atom (fact_head, fact_body))
        (string_of_subs_opt res);
        *)
        res
    ;;

    let unify_entity (ground : Entity.t) e a v t =
        (* TODO - Convert Rule.Pattern into something more specific to make these checks easier *)
        let value = match ground.value with
                  | `ERef i -> `Int i
                  | `Val e -> `Val e
        in
        let zip_lst = [`Int ground.id, e; `Int ground.attr, a; value, v; `Int ground.tid, t] in
        let rec loop subs = function
            | [] -> Some subs
            | (_, `Ignore) :: rest -> loop subs rest
            | (`Val v, `Val y) :: rest when Value.(equal v y) -> loop subs rest
            | (`Int i, `Int y) :: rest when i = y -> loop subs rest
            | (_, `Val _) :: _
            | (_, `Int _) :: _ -> None
            | ((`Int _ as s), `Var v) :: rest
            | ((`Val _ as s), `Var v) :: rest ->
                 match Sub.lookup subs v with
                 | None -> loop (Sub.extend subs v s) rest
                 | Some l when Sub.(equal_value s l) -> loop subs rest
                 | Some _ -> None
        in
        let res = loop Sub.empty zip_lst in
        (*Printf.printf "Unify entity result %s\n" (_string_of_subs_opt res);*)
        res
    ;;

    let unify_rule (ground_terms : TermList.t) (terms : Rule.Pattern.t list) =
        (*
        let str1 = TermList.to_string ground_terms in
        let str2 = terms |> List.map ~f:Rule.Pattern.show |> String.concat ~sep:" " in
        Printf.printf "Unifying rule %s - %s\n" str1 str2;
        *)
       let rec loop subs = function
           | [] -> Some subs
           | (_, `Ignore) :: rest -> loop subs rest
           | (`Val v, `Val y) :: rest when Value.(equal v y) -> loop subs rest
           | (`Int i, `Int y) :: rest when i = y -> loop subs rest
           | (_, `Val _) :: _
           | (_, `Int _) :: _ -> None
           | (s, `Var v) :: rest ->
                match Sub.lookup subs v with
                | None -> loop (Sub.extend subs v s) rest
                | Some l when Sub.(equal_value s l) -> loop subs rest
                | Some _ -> None
       in
       let res = loop Sub.empty List.(zip_exn ground_terms terms) in
       (*Printf.printf "Result %s\n" (string_of_subs_opt res);*)
       res
    ;;

end
