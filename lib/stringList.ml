module T = struct
    type t = string list
    [@@deriving compare, sexp, hash, equal, bin_io]

    let of_terms lst =
        lst |> List.map ~f:(function
            | `Val _ as v -> v
            | `Int _ as i -> i
            | _ -> failwith "Expected literal"
        )
end

include T
include Comparable.Make(T)

