module type S = sig
    module type KeySig = sig
      type t
      [@@deriving sexp, compare, equal, bin_io]
    end

    module IndexMap(KeyS : KeySig) : sig
        type t
        type key = KeyS.t

        val create : child_count:int -> t

        val insert : t -> key -> t

        val merge_with_fn : t -> t -> f:(key -> 'b) -> t

        val get_range_start : t -> KeyS.t -> KeyS.t Base.Sequence.t

        val to_string : t -> string
    end

    module EAVTIndex : sig type t end
    module AEVTIndex : sig type t end
    module AVETIndex : sig type t end
    module VAETIndex : sig type t end

    type t = {
        eavt : EAVTIndex.t;
        aevt : AEVTIndex.t;
        avet : AVETIndex.t;
        vaet : VAETIndex.t;
    }[@@deriving sexp, compare, equal, bin_io]
end

module Make(Value : Sigs.Value)(Entity : module type of Entity.Make(Value)) = struct
    module Rule = Rule.Make(Value)

    let compare_value v1 v2 =
        match v1, v2 with
        | `ERef r1, `ERef r2 -> Int.compare r1 r2
        | `Val v1, `Val v2 -> Value.compare v1 v2
        | `ERef _, `Val _ -> -1
        | `Val _, `ERef _ -> 1

    let int_cmp a b =
        Int.compare a b
        (*if a < b then -1 else if a > b then 1 else 0*)

    module EAVTKey = struct
        type t = Entity.t
        [@@deriving sexp, compare, equal, bin_io]

        let compare (e1 : Entity.t) (e2 : Entity.t) =
            let cmp = (*Int.compare e1.id e2.id in*) int_cmp e1.id e2.id in
            if cmp = 0 then (
                let cmp = int_cmp e1.attr e2.attr in
                if cmp = 0 then (
                    let cmp = compare_value e1.value e2.value in
                    if cmp = 0 then Int.compare e2.tid e1.tid
                    else cmp
                ) else cmp
            ) else cmp
    end

    module AEVTKey = struct
        type t = Entity.t
        [@@deriving sexp, compare, equal, bin_io]

        let compare (e1 : Entity.t) (e2 : Entity.t) =
            let cmp = int_cmp e1.attr e2.attr in
            if cmp = 0 then (
                let cmp = Int.compare e1.id e2.id in
                if cmp = 0 then (
                    let cmp = compare_value e1.value e2.value in
                    if cmp = 0 then Int.compare e2.tid e1.tid
                    else cmp
                ) else cmp
            ) else cmp
    end

    module AVETKey = struct
        type t = Entity.t
        [@@deriving sexp, compare, equal, bin_io]

        let compare (e1 : Entity.t) (e2 : Entity.t) =
            let cmp = int_cmp e1.attr e2.attr in
            if cmp = 0 then (
                let cmp = compare_value e1.value e2.value in
                if cmp = 0 then (
                    let cmp = Int.compare e1.id e2.id in
                    if cmp = 0 then Int.compare e2.tid e1.tid
                    else cmp
                ) else cmp
            ) else cmp
    end

    module VAETKey = struct
        type t = Entity.t
        [@@deriving sexp, compare, equal, bin_io]

        let compare (e1 : Entity.t) (e2 : Entity.t) =
            let res =     
                let cmp = compare_value e1.value e2.value in
                if cmp = 0 then (
                    let cmp = int_cmp e1.attr e2.attr in
                    if cmp = 0 then (
                        let cmp = Int.compare e1.id e2.id in
                        if cmp = 0 then Int.compare e2.tid e1.tid
                        else cmp
                    ) else cmp
                ) else cmp
            in
            (*
            Printf.printf "%s = %s, %d\n" 
                (Entity.to_string e1)
                (Entity.to_string e2)
                res;
                *)
                res
    end

    module type KeySig = sig
      type t
      [@@deriving sexp, compare, equal, bin_io]
    end

    module IndexMap(KeyS : KeySig) = struct
        include Set.Make(KeyS)
        include Provide_bin_io(KeyS)

        let create ~child_count:_ = empty

        let insert t key =
           (* set t ~key ~data:node*)
            add t key

        let merge_with_fn (t1 : t) (t2 : t) ~f =
            fold t2 ~init:t1 ~f:(fun set elt ->
                insert set (f elt)
            )
        ;;

        let get_range_start node key =
            to_sequence ~order:`Increasing ~greater_or_equal_to:key node

        let to_string t =
            t |> sexp_of_t |> Sexp.to_string_hum
    end

    module EAVTIndex = IndexMap(EAVTKey)
    module AEVTIndex = IndexMap(AEVTKey)
    module AVETIndex = IndexMap(AVETKey)
    module VAETIndex = IndexMap(VAETKey)

    type t = {
        eavt : EAVTIndex.t;
        aevt : AEVTIndex.t;
        avet : AVETIndex.t;
        vaet : VAETIndex.t;
    }[@@deriving sexp, compare, equal, bin_io]

    let merge (t1 : t) (t2 : t) = {
        eavt = EAVTIndex.union t1.eavt t2.eavt;
        aevt = AEVTIndex.union t1.aevt t2.aevt;
        avet = AVETIndex.union t1.avet t2.avet;
        vaet = VAETIndex.union t1.vaet t2.vaet;
    }

    let merge_with_fn (t1 : t) (t2 : t) ~f = {
        eavt = EAVTIndex.merge_with_fn t1.eavt t2.eavt ~f;
        aevt = AEVTIndex.merge_with_fn t1.aevt t2.aevt ~f;
        avet = AVETIndex.merge_with_fn t1.avet t2.avet ~f;
        vaet = VAETIndex.merge_with_fn t1.vaet t2.vaet ~f;
    }

    let default_index_size = 10000

    let empty = {
        eavt = EAVTIndex.create ~child_count:default_index_size;
        aevt = AEVTIndex.create ~child_count:default_index_size;
        avet = AVETIndex.create ~child_count:default_index_size;
        vaet = VAETIndex.create ~child_count:default_index_size;
    }

    let insert (index : t) entities =
        Entity.Set.fold entities ~init:index ~f:(fun index entity ->
            let eavt = EAVTIndex.insert index.eavt entity in
            let aevt = AEVTIndex.insert index.aevt entity in
            let avet = AVETIndex.insert index.avet entity in
            let vaet = VAETIndex.insert index.vaet entity in
            { eavt; aevt; avet; vaet; }
        )
    ;;

    let remove (index : t) entities =
        Entity.Set.fold entities ~init:index ~f:(fun index entity ->
            let eavt = EAVTIndex.remove index.eavt entity in
            let aevt = AEVTIndex.remove index.aevt entity in
            let avet = AVETIndex.remove index.avet entity in
            let vaet = VAETIndex.remove index.vaet entity in
            { eavt; aevt; avet; vaet; }
        )
    ;;

    let match_int ?(base=Int.min_value) = function
         | `Var _
         | `Ignore -> base
         | `Int id -> id
         (*| `Val (`Int id) -> id*)
         | x -> failwith ("Unexpected id pattern " ^ Rule.Pattern.show x)
    ;;

    let match_value = function
          | `Var _
          | `Ignore -> `Val Value.min_value
          | `Int i -> `ERef i
          | `Val _ as v -> v
    ;;

    let string_pat p1 p2 p3 p4 =
        String.concat ~sep:" " Rule.Pattern.[show p1; show p2; show p3; show p4]

    let debug = false

    let print_endline str =
        if debug then Caml.print_endline str

    let find_matches t (p1 : Rule.Pattern.t) (p2 : Rule.Pattern.t) (p3 : Rule.Pattern.t) (p4 : Rule.Pattern.t) =
        (* Must construct a key *)
        let id = match_int p1 in
        let attr = match_int p2 in
        let value = match_value p3 in
        let tid = match_int ~base:Int.max_value p4 in
        let key = Entity.{id; attr; value; tid; added=true} in
        (*Printf.printf "Pattern %s\n Entity key %s\n" (string_pat p1 p2 p3 p4) Entity.(to_string key);*)
        (* Pick most relevant index to search..., such that we can have
         * all the relevant results in sorted order and first rejection
         * means we have found all keys that could possibly match...
         *)
        match p1, p2, p3, p4 with
        | _, `Int _, `Val _, _
        | _, `Int _, `Int _, _ ->
                print_endline "vaet";
                VAETIndex.get_range_start t.vaet key
        | `Int _, _, _, _
        | `Val _, _, _, _ ->
                print_endline "eavt";
                EAVTIndex.get_range_start t.eavt key
        | _, `Int _, _, _
        | _, `Val _, _, _ ->
                print_endline "aevt";
                AEVTIndex.get_range_start t.aevt key
        | _, _, `Val _, _ ->
                print_endline "vaet";
                VAETIndex.get_range_start t.vaet key
        | _, _, _, `Val _ ->
                print_endline "eavt";
                EAVTIndex.get_range_start t.eavt key
        | _ ->
                print_endline "eavt";
                EAVTIndex.get_range_start t.eavt key
    ;;
end
