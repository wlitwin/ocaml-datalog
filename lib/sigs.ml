module type Value = sig
    type t
    [@@deriving sexp, equal, compare, hash, show{with_path=false}, bin_io]

    val to_string : t -> string
    val symbol_name : t -> string

    val cardinal_one : t
    val cardinal_many : t

    (* TODO - create a third type for the DB ? to store types? *)
    module Types : sig
        type value = t

        type t
        [@@deriving sexp, equal, compare, hash, show{with_path=false}, bin_io]

        val db_symbol : t
        val db_type : t
        val db_ref : t
        val db_cardinality : t
        val db_timestamp : t
        val db_string : t
        val db_user : t
        val db_int : t
        val to_value : t -> value
        val of_value : value -> t

        val is_valid : t -> value -> bool
        val is_symbol : value -> bool
        val is_timestamp : value -> bool
        val is_user : value -> bool
    end

    val of_symbol : string -> t
    val of_int : int -> t
    val of_time : int -> t
    val of_string : string -> t

    val min_value : t
end
