module Make(Value : Sigs.Value) = struct
    type t = 
          [ `Var of int
          | `Val of Value.t
          | `Int of int
          ]
          [@@deriving compare, sexp, hash, equal]

    let to_string = function
        | `Var v -> "?" ^ Int.to_string v
        | `Int i -> Int.to_string i ^ "i"
        | `Val v -> Value.to_string v

    module ConcreteTermList = struct
        type values = [
                      | `Val of Value.t
                      | `Int of int
                      ]
                      [@@deriving compare, sexp, hash, equal, bin_io]

        module T = struct
            type t = values list
            [@@deriving compare, sexp, hash, equal]
        end

        include T
        include Comparable.Make(T)

        let to_string terms =
            terms |> List.map ~f:to_string |> String.concat ~sep:", "
    end

    module List = struct
        module T = struct
            type term = t
            [@@deriving compare, sexp, hash, equal]

            type t = term list
            [@@deriving compare, sexp, hash, equal]
        end

        include T
        include Comparable.Make(T)
    end
end
