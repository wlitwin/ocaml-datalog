module Make(Value : Sigs.Value) = struct
    module Types = Value.Types
    module Entity = Entity.Make(Value)

    module T = struct
        module Attr = struct
            type cardinality = One | Many
            [@@deriving sexp, bin_io, compare, equal]

            (* TODO - maybe allow optional eid 
             * but nothing stops someone from using
             * the expert datom_full to do it...
             * *)
            type t = {
                eid : int;
                name : string;
                type_ : Types.t;
                cardinality : cardinality;
            }[@@deriving sexp, bin_io, compare, equal]

            let cardinality (t : t) : Value.t =
                match t.cardinality with
                | One -> Value.cardinal_one
                | Many -> Value.cardinal_many
        end

        type eid = Rel of int
                 | Abs of int
        type attr = string
        type value = Ref of eid
                   | Val of Value.t

        type datom = {
            eid : eid;
            attr : attr;
            value : value;
            tid : int option;
        }

        type nonrec t = {
            next_eid : int;
            attributes : Attr.t list;
            datoms : datom list;
            txn_attrs : (string * value) list;
            deterministic_txn_timestamp : bool;
        }

        let attribute (t : t) name card type_ =
            { t with attributes =
                    Attr.{
                        eid = -1;
                        name;
                        cardinality = card;
                        type_;
                    } :: t.attributes
            }
        ;;

        let empty = {
            next_eid = 0;
            attributes = [];
            datoms = [];
            txn_attrs = [];
            deterministic_txn_timestamp = false;
        }

        let empty_deterministic = {
            empty with
            deterministic_txn_timestamp = true
        }

        let deterministic_txn_timestamp t b =
            { t with deterministic_txn_timestamp = b }


        let make_id (t : t) : eid * t =
            Rel t.next_eid, {t with next_eid = t.next_eid+1}
        ;;

        let datom t eid attr value = {
            t with datoms = { eid; attr; value; tid = None} :: t.datoms
        }

        let datom_full t eid attr value tid = {
            t with datoms = { eid; attr; value; tid = Some tid} :: t.datoms
        }

        let eid_of_int id = Abs id

        let add_txn_attribute t attr value = {
            t with txn_attrs = (attr, value) :: t.txn_attrs
        }

        let create_attribute (attr : Attr.t) strings next_eid next_tid =
            let strings = String.Map.add_exn strings ~key:attr.name ~data:{attr with eid=next_eid} in
            let find_id name = (String.Map.find_exn strings name).eid in
            let e1 = Entity.{
                id = next_eid;
                attr = find_id "db/attribute";
                value = `Val Value.(of_symbol attr.name);
                tid = next_tid;
                added = true;
            } in
            let e2 = Entity.{
                id = next_eid;
                attr = find_id "db/cardinality";
                value = `Val Attr.(cardinality attr);
                tid = next_tid;
                added = true;
            } in
            let e3 = Entity.{
                id = next_eid;
                attr = find_id "db/valueType";
                value = `Val Types.(to_value attr.type_);
                tid = next_tid;
                added = true
            } in
            (strings, e1, e2, e3)
        ;;
    end

    include T
end
