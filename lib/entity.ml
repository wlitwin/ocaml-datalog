module Make(Value : Sigs.Value) = struct
    module T = struct
        type value = [
            | `Val of Value.t
            | `ERef of int
        ][@@deriving compare, hash, sexp, show{with_path=false}, bin_io]

        let string_of_value : value -> string = function
            | `Val e -> Value.to_string e
            | `ERef i -> "`" ^ Int.to_string i

        type t = {
            id : int; (* Entity ref *)
            attr : int; (* interned string id *) (* TODO - make ref to attribute eid *)
            value : value;
            tid : int;
            added : bool;
        }[@@deriving compare, hash, sexp, show{with_path=false}, bin_io]

        let make id attr value tid added = {
            id; attr; value; tid; added;
        }

        let equal_ignore_tid e1 e2 =
            if Int.equal e1.id e2.id then
                if Int.equal e1.attr e2.attr then
                    compare_value e1.value e2.value = 0
                else false
            else false
        ;;

        let offset_by t ~offset = 
            let value = match t.value with
                      | `Val _ as v -> v
                      | `ERef e -> `ERef (e + offset)
            in
            {
                id = t.id + offset;
                attr = t.attr + offset;
                value;
                tid = t.tid + offset;
                added = t.added;
            }
        ;;

        let to_string = show
    end

    include T
    include Comparable.Make(T)

    module Set = struct
        include Set
        include Set.Provide_bin_io(T)
    end

    module Hash_set = Hash_set.Make(T)
    module Hashtbl = Hashtbl.Make(T)
end
