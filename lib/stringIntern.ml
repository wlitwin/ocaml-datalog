module StringTree = struct
    include Map.Make(String)
    include Provide_bin_io(String)
end

type t = int StringTree.t
[@@deriving sexp, bin_io, compare, equal]

let empty = StringTree.empty

let offset_by t ~offset = 
    StringTree.map t ~f:(fun item ->
        item + offset
    )
;;

let intern_fn t ~f str =
    match StringTree.find t str with
    | None -> 
        let id = f() in
        id, StringTree.set t ~key:str ~data:id
    | Some id -> id, t

let intern t ~attr_id str =
    match StringTree.find t str with
    | Some id -> id, t
    | None -> attr_id, StringTree.set t ~key:str ~data:attr_id
;;

exception Intern_exists of string
let intern_exn t ~attr_id str =
    match StringTree.find t str with
    | Some _ -> raise (Intern_exists str)
    | None -> StringTree.set t ~key:str ~data:attr_id
;;

let merge (t1 : t) (t2 : t) = 
    StringTree.merge t1 t2 ~f:(fun ~key -> function
        | `Both (a, b) -> 
                if not (a = b) then (
                    Printf.printf "%s = %d | %d\n" key a b;
                    assert (a = b);
                );
                Some a
        | `Left a -> Some a
        | `Right b -> Some b
    )

let find_exn = StringTree.find_exn
let find = StringTree.find

let to_string t =
    StringTree.fold t ~init:"" ~f:(fun ~key ~data str ->
        str ^ "\n" ^ key ^ " = " ^ Int.to_string data
    )
