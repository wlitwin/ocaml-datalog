module Make(Value : Sigs.Value) = struct
    module Atom = Atom.Make(Value)
    module Rule = Rule.Make(Value)
    module Term = Term.Make(Value)
    module Entity = Entity.Make(Value)
    module Index = Index.Make(Value)(Entity)
    module KBE = KnowledgeBase.Make(Value)
    module Value = Value
end
