(* Top-level Rule language
 *
 * rule := name [clause]
 * clause = rule_ref
 *         | pattern
 *         | not clause
 *         | or [clause | and-clause]
 * and-clause = and [clause]
 *
 * Gets translated into a simpler set of rules, or-clauses
 * are just temporary rules.
 *)
module Make(Value : Sigs.Value) = struct
    module Atom = Atom.Make(Value)
    module Term = Term.Make(Value)
    module Sub = Sub.Make(Value)

    module Cond = struct
        type exp =
               [ `Var of int
               | `Val of Value.t
               | `Int of int
               ][@@deriving equal, compare, hash, show{with_path=false}]

        type t = Lt
               | Gt
               | Lte
               | Gte
               | Eq
               | Neq
               [@@deriving equal, compare, hash, show{with_path=false}]

        let substitute (subs : Sub.t) = function
            | `Int _ as i -> i
            | `Val _ as v -> v
            | `Var x as var ->
                match Sub.lookup subs x with
                | None -> var
                | Some (`Int v) -> `Int v
                | Some (`Val v) -> `Val v

        let mk_op i_op : exp * exp -> bool = function
            | `Int i1, `Int i2 -> i_op i1 i2
            | `Val v1, `Val v2 -> i_op (Value.compare v1 v2) 0
            | `Var _, _ -> failwith "Op left side var"
            | _, `Var _ -> failwith "Op right side var"
            | _ -> failwith "Invalid arg to op"

        let lte = mk_op (<=)
        let lt = mk_op (<)
        let gte = mk_op (>=)
        let gt = mk_op (>)
        let eq = mk_op (=)
        let neq = mk_op (<>)

        let get_op = function
            | Lt -> lt
            | Lte -> lte
            | Gt -> gt
            | Gte -> gte
            | Eq -> eq
            | Neq -> neq
    end

    module Pattern = struct
        type t = 
               [ `Ignore
               | `Var of int
               | `Val of Value.t
               | `Int of int
               ]
               [@@deriving equal, hash, show{with_path=false}]

        type func_arg =
               [ `Var of int
               | `Val of Value.t
               | `Int of int
               ][@@deriving equal, hash, show{with_path=false}, compare]

        let compare (a : t) (b : t) =
            match a, b with
            | `Ignore, `Ignore -> 0
            | `Ignore, _ -> 1
            | _, `Ignore -> -1
            | `Var _, `Var _ -> 0
            | `Var _, _ -> 1
            | _, `Var _ -> -1
            | `Int _, `Int _ -> 0
            | `Int _, _ -> -1
            | _, `Int _ -> 1
            | `Val _, `Val _ -> 0

        let is_literal = function
            | `Ignore | `Var _ -> false
            | `Int _ | `Val _ -> true

        let gather_vars lst =
            List.filter_map lst ~f:(function
                | `Ignore | `Int _ | `Val _ -> None
                | `Var v -> Some v
            ) |> Int.Set.of_list

        let is_term = function
            | #Term.t -> true
            | _ -> false

        let all_terms = List.for_all ~f:is_term

        let cast_terms pats =
            List.map pats ~f:(function
                | #Term.t as t -> t
                | _ -> failwith "Expected term"
            )

        let substitute (subs : Sub.t) = function
            | `Int _ as i -> i
            | `Val _ as v -> v
            | `Ignore as i -> i
            | `Var x as var ->
                match Sub.lookup subs x with
                | None -> var
                | Some (`Int v) -> `Int v
                | Some (`Val v) -> `Val v
        ;;

        let substitute_fun (subs : Sub.t) = function
            | `Int _ as i -> i
            | `Val _ as v -> v
            | `Var x as var ->
                match Sub.lookup subs x with
                | None -> var
                | Some (`Int v) -> `Int v
                | Some (`Val v) -> `Val v
        ;;
    end

    module Clause = struct
        type t = RuleRef of string * Pattern.t list
               | Cond of Cond.t * Cond.exp * Cond.exp
               (* TODO - turn func into a struct and insert the function directly *)
               | Func of int * Pattern.func_arg list * int (*var*) list
               | Atom of Pattern.t * Pattern.t * Pattern.t * Pattern.t
               [@@deriving equal, compare, hash, show{with_path=false}]

        let gather_vars = function
            | RuleRef (_, pats) -> Pattern.gather_vars pats
            | Cond (_, e1, e2) -> Pattern.gather_vars [e1;e2]
            | Func (_, args, outs) -> 
                    (* TODO - do we need the out args? *)
                    Int.Set.union
                    Pattern.(gather_vars args)
                    (Int.Set.of_list outs)
            | Atom (p1, p2, p3, p4) -> Pattern.gather_vars [p1;p2;p3;p4]

        let is_ground = function
            | RuleRef (_, pats) -> List.for_all pats ~f:Pattern.is_literal
            | Func (_, args, _) -> List.for_all args ~f:Pattern.is_literal
            | Cond (_, e1, e2) -> Pattern.(is_literal e1 && is_literal e2)
            | Atom (p1, p2, p3, p4) ->
                Pattern.(is_literal p1 && is_literal p2 && is_literal p3 && is_literal p4)

        let substitute_func_args (subs : Sub.t) lst =
            List.map lst ~f:(fun value ->
                match value with
                | `Int _ as i -> i
                | `Val _ as v -> v
                | `Var x ->
                    match Sub.lookup subs x with
                    | None -> failwith "Expected resolved variable for func arg"
                    | Some (`Int v) -> `Int v
                    | Some (`Val v) -> `Val v
            )
        ;;

        let substitute_vars (subs : Sub.t) = 
            let sub = Pattern.substitute subs in
            let sub_fun = Pattern.substitute_fun subs in
            function
            | RuleRef (name, pats) -> RuleRef (name, List.map ~f:sub pats)
            | Func (i, args, outs) -> 
                let args = List.map ~f:sub_fun args in
                Func (i, args, outs)
            | Cond (c, e1, e2) -> Cond (c, Cond.substitute subs e1, Cond.substitute subs e2)
            | Atom (p1, p2, p3, p4) -> Atom (sub p1, sub p2, sub p3, sub p4)

        let to_string = show
    end

    module Func = struct
        type concrete = 
                [ `Int of int 
                | `Val of Value.t
                ]

        (* TODO - functions might need to "fail" on certain
         *        inputs? Maybe change result parameter to
         *        Ok of concrete list | Fail 
         *
         *        So that it can remove certain substitutions
         *)
        type t = Predicate of (concrete list -> bool)
               | General of (concrete list -> concrete list)

        type map = t Int.Map.t

        let empty = Int.Map.empty
    end

    type pos = Pos | Not
        [@@deriving equal, compare, hash]

    type rule_clause = pos * Clause.t
        [@@deriving equal, compare, hash]

    let is_positive : rule_clause -> bool = function
        | Pos, _ -> true
        | Not, _ -> false

    let is_negative = Fn.compose not is_positive

    let string_of_clause = function
        | Pos, a -> "+" ^ Clause.to_string a
        | Not, a -> "-" ^ Clause.to_string a

    type t = Atom.t * rule_clause list
    [@@deriving equal, compare, hash]

    let to_string ((head, body): t) : string =
        let head = Atom.to_string head in
        let body = List.map ~f:string_of_clause body in
        head ^ "\n  " ^ String.concat ~sep:"\n  " body
    ;;

    let clause_atom = function
        | Not, a -> a
        | Pos, a -> a

    let is_range_restricted (rule : t) : bool =
        let head, body = rule in
        let head_vars = Atom.gather_vars head in
        let body_vars =
            List.fold ~init:Int.Set.empty ~f:(fun set clause -> 
                let clause_vars = 
                    clause
                    |> clause_atom
                    |> Clause.gather_vars
                in
                Int.Set.union set clause_vars
            ) body
        in
        Int.Set.(is_subset head_vars ~of_:body_vars)
    ;;

    let name ((head, _), _ : t) : string = head

    (* TODO - what order do functions go in? 
     * functions have input dependencies that must
     * be full resolved and has outputs. The outputs
     * may be depended on by conditional clauses, rules
     * or atoms. Might also be possible to not order
     * them properly ?
     * *)
    let reorder_clauses (rule : t) =
        let open Clause in
        let compare r1 r2 =
            match r1, r2 with
            | (_, (Cond _ as c1)), (_, (Cond _ as c2)) -> Clause.compare c1 c2
            | (_, (Cond _)), (_, (Atom _)) -> 1
            | (_, (Atom _)), (_, (Cond _)) -> -1
            | (_, (Atom _)), (_, (Func _)) -> 0
            | (_, (Func _)), (_, (Atom _)) -> 0
            | (_, (Cond _)), _ -> 1
            | _, (_, (Cond _)) -> 1
            | (Pos, _), (Not, _) -> -1
            | (Not, _), (Pos, _) -> 1
            | (Pos, a1), (Pos, a2) -> Clause.compare a1 a2
            | (Not, a1), (Not, a2) -> Clause.compare a1 a2
        in
        let head, body = rule in
        let body = List.sort body ~compare in
        head, body
    ;;

    let is_ground ((head, body) : t) : bool =
        let body = List.map ~f:clause_atom body in
        Atom.is_ground head && List.for_all body ~f:Clause.is_ground
    ;;

    (* Non-recursive, doesn't reference other rules *)
    let is_simple (rule_set : String.Set.t) (rule : t) : bool =
        let _, body = rule in
        List.exists body ~f:(fun clause ->
            match clause_atom clause with
            | Clause.Atom _ -> false
            | Cond _ -> false
            | Func _ -> true
            | RuleRef (head, _) -> String.Set.mem rule_set head
        ) |> not
    ;;

    let has_no_refs ((_, body) :t) : bool =
        List.exists body ~f:(fun clause ->
            match clause_atom clause with
            | Clause.Atom _ -> false
            | Func _ -> true
            | Cond _ -> false
            | RuleRef _ -> true
        ) |> not
    ;;

    let is_safe (_rule : t) : bool =
        (* TODO all not'd variables must also
         * occur in a positive position...
         * + must be range restricted
         *)
        true
    ;;

    let rule_name_set (rules : t list) =
        List.fold ~init:String.Set.empty ~f:(fun set rule ->
            String.Set.add set (name rule)
        ) rules
    ;;
end
