(* TODO - change from immutable to mutable, and change from
 *        maps to static arrays, as the number of rules
 *        does not change during a query we can do a
 *        constant time lookup
 *)
module Make(Value : Sigs.Value) = struct
    module Atom = Atom.Make(Value)
    module Term = Term.Make(Value)
    module TermList = Term.ConcreteTermList

    type t = {
        rule_map : TermList.Set.t String.Map.t; (* Can be just the subs, should be ground terms only *)
    }

    let are_rules_empty t =
        String.Map.for_alli t.rule_map ~f:(fun ~key:_ ~data ->
            TermList.Set.is_empty data
        )

    let pats_to_key _pats =
        []
    ;;

    let fold_rules (t : t) (name : string) pats =
        match String.Map.find t.rule_map name with
        | None -> Sequence.empty
        | Some set -> 
            (* Jump to the relevant sorted keys *)
            TermList.Set.to_sequence ~order:`Increasing ~greater_or_equal_to:(pats_to_key pats) set
    ;;

    let empty = {
        rule_map = String.Map.empty;
    }

    let merge (t1 : t) (t2 : t) : t = {
        rule_map = Map.merge_skewed t1.rule_map t2.rule_map ~combine:(fun ~key:_ s1 s2 ->
            TermList.Set.union s1 s2
        )
        (*
        rule_map = Map.merge t1.rule_map t2.rule_map ~f:(fun ~key:_ data ->
            match data with
            | `Both (a, b) -> Some (TermList.Set.union a b)
            | `Left a -> Some a
            | `Right a -> Some a
        )
    *)
    }

    let add_rule (t : t) ((head, terms) : Atom.t) = 
        let terms = StringList.of_terms terms in
        let rule_map = match String.Map.find t.rule_map head with
            | None -> String.Map.set t.rule_map ~key:head ~data:(TermList.Set.singleton terms)
            | Some set -> String.Map.set t.rule_map ~key:head ~data:(TermList.Set.add set terms)
        in 
        { rule_map }
    ;;

    let rule_to_list (t : t) name =
        match String.Map.find t.rule_map name with
        | None -> []
        | Some set -> TermList.Set.to_list set

    let diff_maps r1 r2 =
        (* TODO - symmetric diff? *)
        String.Map.fold r1 ~init:String.Map.empty ~f:(fun ~key ~data map ->
            match String.Map.find r2 key with
            | None -> String.Map.set map ~key ~data
            | Some data2 -> String.Map.set map ~key ~data:(TermList.Set.diff data data2) 
        )
    ;;

    let diff (t1 : t) (t2 : t) : t = {
        rule_map = diff_maps t1.rule_map t2.rule_map;
    }

    let for_all_rules (t : t) name ~f =
        match String.Map.find t.rule_map name with
        | None -> true
        | Some set -> TermList.Set.for_all set ~f

    let _to_string (t : t) : string =
        (*
        let entity_to_str (e : Entity.t) = 
            Printf.sprintf "(%d, %s, %s)" e.id e.attr (Entity.string_of_value e.value) 
        in
        let ents = t.entities |> Entity.Set.to_list |> List.map ~f:entity_to_str |> String.concat ~sep:"\n" in
    *)
        let rules = 
            t.rule_map 
            |> String.Map.to_alist 
            |> List.map ~f:(fun (key, set) -> 
                key ^ "\n" ^
                (set
                |> TermList.Set.to_list 
                |> List.map ~f:TermList.to_string
                |> String.concat ~sep:"\n"
                )
            ) |> String.concat ~sep:"\n"
        in
        (*ents ^ "\n" ^*) rules
    ;;
end
