module Make(Value : Sigs.Value)
           (Unify : module type of Unify.Make(Value))
           (KBE : module type of KnowledgeBase.Make(Value))
           (Entity : module type of Entity.Make(Value))
           (Index : module type of Index.Make(Value)(Entity)) 
       = struct

    module Sub = Unify.Sub
    module Rule = Unify.Rule
    module Atom = Unify.Atom

    let iters = ref 0

    let string_of_sub_list lst =
        String.concat ~sep:"\n" (List.map ~f:Sub.to_string lst)

    let generate_subs_positive (index : Index.t) (kb : KBE.t) (subs : Sub.t list) (atom : Rule.Clause.t) =
        (*Caml.print_endline "Subs";
        Caml.print_endline (string_of_sub_list subs);*)
        List.fold ~init:[] ~f:(fun acc_subs sub ->
            let clause = Rule.Clause.substitute_vars sub atom in
            assert (not (Rule.Clause.is_ground clause));
            match clause with
            | Func _ -> failwith "Impossible - Func"
            | Cond _ -> failwith "Impossible - Cond"
            | RuleRef (name, pats) ->
                let take_map seq =
                    let rec loop acc seq =
                        incr iters;
                        match Sequence.next seq with
                        | None -> acc
                        | Some (fact, rest) ->
                            match Unify.unify_rule fact pats with
                            | None -> acc
                            | Some facts -> loop ((Sub.merge sub facts) :: acc) rest
                    in
                    loop [] seq
                in
                let seq = KBE.fold_rules kb name pats in
                let matches = take_map seq in
                List.append matches acc_subs

            | Atom (p1, p2, p3, p4) ->
                    (*Printf.printf "%s %s %s %s\n" 
                        Rule.Pattern.(show p1) 
                        Rule.Pattern.(show p2) 
                        Rule.Pattern.(show p3)
                        Rule.Pattern.(show p4);*)
                    (*timeit "entity" (fun () ->*)
                let take_map seq =
                    let rec loop acc seq =
                        incr iters;
                        match Sequence.next seq with
                        | None -> acc
                        | Some (fact, rest) -> 
                            match Unify.unify_entity fact p1 p2 p3 p4 with
                            | None -> acc
                            | Some facts -> loop ((Sub.merge sub facts) :: acc) rest
                    in
                    loop [] seq
                in
                let seq = Index.find_matches index p1 p2 p3 p4 in
                let matches = take_map seq in
                (*Caml.print_endline "Subs";
                Caml.print_endline (string_of_sub_list matches);*)
                List.append matches acc_subs
        ) subs
    ;;

    let generate_subs_negative (index : Index.t) (kb : KBE.t) (subs : Sub.t list) (atom : Rule.Clause.t) =
        List.filter subs ~f:(fun sub -> 
            let clause = Rule.Clause.substitute_vars sub atom in
            (*assert (Rule.Clause.is_ground clause);*)
            match clause with
            | Func _ -> failwith "Impossible - Func"
            | Cond _ -> failwith "Impossible - Cond"
            | RuleRef (name, pats) ->
                KBE.for_all_rules kb name ~f:(fun fact ->
                    match Unify.unify_rule fact pats with
                    | None -> true
                    | Some _ -> false
                )
            | Atom (p1, p2, p3, p4) ->
                let take_map seq =
                    let rec loop seq =
                        incr iters;
                        match Sequence.next seq with
                        | None -> true
                        | Some (fact, rest) -> 
                            match Unify.unify_entity fact p1 p2 p3 p4 with
                            | None -> loop rest
                            | Some _ -> false
                    in
                    loop seq
                in
                let seq = Index.find_matches index p1 p2 p3 p4 in
                take_map seq
        )
    ;;

    (* TODO - should be able to match partials against the KBE?
     *        (COND, VAR, VAL) -> if first, won't match anything
     *)
    let generate_subs_cond (invert : bool) (subs : Sub.t list) (atom : Rule.Clause.t) =
        List.filter subs ~f:(fun sub ->
            (*List.iter ~f:(fun s -> Sub.to_string s |> Caml.print_endline) subs;*)
            let clause = Rule.Clause.substitute_vars sub atom in
            match clause with
            | RuleRef _ | Atom _ | Func _ -> failwith "Impossible"
            | Cond (_, `Var _v, _) -> 
                    (*Printf.printf "cond var %d\n" v;*)
                    true (* This should probably raise? *)
            | Cond (_, _, `Var _v) -> 
                    (*Printf.printf "cond var %d\n" v;*)
                    true
            | Cond (op, (`Val _ as e1), (`Val _ as e2))
            | Cond (op, (`Int _ as e1), (`Int _ as e2)) ->
                (*Printf.printf "cond %s %s\n" Term.(to_string e1) Term.(to_string e2);*)
                let op = Rule.Cond.get_op op in
                if invert then not (op (e1, e2))
                else op (e1, e2)
            | Cond (_, _e1, _e2) -> 
                (*Printf.printf "Cond catch-all %s %s\n" Term.(to_string e1) Term.(to_string e2);*)
                false
        )
    ;;

    let generate_subs_func (funcs : Rule.Func.map) (_invert : bool) (subs : Sub.t list) (func : Rule.Clause.t) =
        (* Invert is only applicable to Predicate type functions... 
         * This should be checked at query start...
         * Function arity should also be verified
         *)
        (* TODO - should we remake the rule grammer? NOT doesn't
         * make sense on General functions, only predicates.
         * this means there should probably be a stand alone
         * NOT rule that has selective members nested. NOT only
         * nests one level deep.
         *)
        match func with
        | RuleRef _ | Atom _ | Cond _ -> failwith "Impossible"
        | Func (id, args, outs) ->
            match Int.Map.find_exn funcs id with
            | Predicate fn -> 
                List.filter subs ~f:(fun sub ->
                    let args = Rule.Clause.substitute_func_args sub args in
                    fn args
                )
            | General fn -> 
                List.fold ~init:[] ~f:(fun acc_subs sub ->
                    let args = Rule.Clause.substitute_func_args sub args in
                    let result = fn args in
                    (* Now generate the subs *)
                    let new_subs = Sub.of_alist List.(zip_exn outs result) in
                    Sub.(merge new_subs sub) :: acc_subs
                ) subs
    ;;

    let generate_subs_for_atom (index : Index.t) (funcs : Rule.Func.map) (kb : KBE.t) (subs : Sub.t list) (clause : Rule.rule_clause) = 
        (* Can probably add negation easily by re-ordering so all NOTs
         * are last and inverting this unify result *)
        (*Caml.print_endline "SUBS HERE?";
        List.iter ~f:(fun s -> Sub.to_string s |> Caml.print_endline) subs;*)
        match clause with
        | pos, (Func _ as func) -> generate_subs_func funcs Poly.(pos = Not) subs func
        | pos, (Cond _ as cond) -> generate_subs_cond Poly.(pos = Not) subs cond
        | Pos, atom -> generate_subs_positive index kb subs atom
        | Not, atom -> generate_subs_negative index kb subs atom
    ;;

    let generate_subs (index : Index.t) (funcs : Rule.Func.map) (kb : KBE.t) (clauses : Rule.rule_clause list) : Sub.t list =
        List.fold clauses ~init:[Sub.empty] ~f:(fun subs ->
            generate_subs_for_atom index funcs kb subs
        )
    ;;

    let substitute_rule (index : Index.t) (funcs : Rule.Func.map) (kb : KBE.t) ((head_atom, clauses) : Rule.t) : KBE.t =
        let subs = generate_subs index funcs kb clauses in
        (*Caml.print_endline (string_of_sub_list subs);
        Caml.print_endline KBE.(_to_string kb);*)
        List.fold subs ~init:KBE.empty ~f:(fun set sub ->
            let atom = Unify.substitute_vars head_atom sub in
            if Atom.is_ground atom then (
                KBE.add_rule set atom
            ) else (
                set
            )
        )
    ;;

    let eval_rules (index : Index.t) (funcs : Rule.Func.map) (kb : KBE.t) (rules : Rule.t list) =
        List.fold ~init:KBE.empty ~f:(fun kbe rule ->
            KBE.merge kbe (substitute_rule index funcs kb rule)
        ) rules
    ;;

end
