module Make(Value : Sigs.Value) = struct
    type value = 
        [ `Val of Value.t
        | `Int of int
        ][@@deriving equal]

    type t = value Int.Map.t
    let empty : t = Int.Map.empty
    let lookup (t : t) (x : int) : value option =
        Int.Map.find t x

    let filter_keys = Int.Map.filter_keys

    let merge (t1 : t) (t2 : t) : t =
        Int.Map.merge t1 t2 ~f:(fun ~key:_ data ->
            match data with
            | `Both (a, _) -> Some a
            | `Left a -> Some a
            | `Right a -> Some a
        )

    let extend (t : t) (x : int) (v : value) : t =
        Int.Map.set t ~key:x ~data:v

    let of_alist (lst : (int * value) list) =
        List.fold lst ~init:empty ~f:(fun set (var, value) ->
            extend set var value
        )
    ;;

    let string_of_value = function
        | `Int i -> Int.to_string i
        | `Val e -> Value.to_string e

    let to_string (t : t) : string =
        t 
        |> Int.Map.to_alist
        |> List.map ~f:(fun (x, y) -> Int.to_string x ^ "=>" ^ (string_of_value y))
        |> String.concat ~sep:"; "
        |> Printf.sprintf "[%s]"
end
