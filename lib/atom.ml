module Make(Value : Sigs.Value) = struct
    module Term = Term.Make(Value)

    (* Need to change so that we can specify multiple DBs and
     * have aggregations in the head 
     *
     * query := rule_name [input]
     * input = agg_fn var | var
     * agg_fn = min count?
     *        | max count?
     *        | sum
     *        | avg
     *        | median
     *        | count
     *        | count-distinct
     *        | variance
     *        | stddev
     *        | sample count?
     *        | random count?
     *)

    module T = struct
        type t = string * (Term.t list)
        [@@deriving compare, sexp, hash, equal]
    end

    include T
    include Comparable.Make(T)
    module Hash_set = Hash_set.Make(T)
    module Hashtbl = Hashtbl.Make(T)

    let gather_vars ((_, terms) : t) =
        List.filter_map terms ~f:(function
            | `Var x -> Some x
            | `Int _
            | `Val _ -> None
        ) |> Int.Set.of_list

    let to_string ((pred, terms) : t) : string =
        Printf.sprintf "%s(%s)" pred (terms |> List.map ~f:Term.to_string |> String.concat ~sep:", ")
    ;;

    let all_syms (terms : Term.t list) : bool =
        List.for_all terms ~f:(function
            | `Int _ | `Val _ -> true
            | `Var _ -> false
        )
    ;;

    let is_ground ((_, terms) : t) : bool =
        all_syms terms
    ;;
end
