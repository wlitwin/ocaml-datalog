module Make : functor (Value : Sigs.Value) -> sig
    module Atom : module type of Atom.Make(Value)
    module Rule : module type of Rule.Make(Value)
    module Sub : module type of Sub.Make(Value)
    module Types : module type of Value.Types with type t := Value.Types.t
    module Func : module type of Rule.Func

    type t

    module Transaction : sig
        type eid
        type attr = string
        type value = Ref of eid
                   | Val of Value.t

        module Attr : sig
            type cardinality = One | Many
            [@@deriving sexp, bin_io, equal, compare]

            type t = {
                eid : int;
                name : string;
                type_ : Value.Types.t;
                cardinality : cardinality;
            }[@@deriving sexp, bin_io, equal, compare]

            val cardinality : t -> Value.t
        end

        type datom = {
            eid : eid;
            attr : attr;
            value : value;
            tid : int option;
        }

        type t
        val empty : t
        val empty_deterministic : t
        val deterministic_txn_timestamp : t -> bool -> t
        val make_id : t -> (eid * t)
        val datom : t -> eid -> attr -> value -> t
        val datom_full : t -> eid -> attr -> value -> int -> t
        val eid_of_int : int -> eid
        val attribute : t -> string -> Attr.cardinality -> Value.Types.t -> t
        val add_txn_attribute : t -> string -> value -> t
    end

    module Txn = Transaction

    type attribute_map = Txn.Attr.t String.Map.t

    type db_stats = {
        interned_strings : attribute_map; 
        eavt_size : int;
    }

    val get_stats : t -> db_stats

    type property = Single of Value.t
                  | Multi of Value.t list

    type entity = {
        id : int;
        attributes : property String.Map.t
    }

    val get_entity : t -> int -> entity option

    type result = Sub.t list
    type query = Atom.t * Func.map
    val empty : t
    val string_of_result : result -> string
    val to_string : t -> string
    val time_now : unit -> int
    val attributes : t -> attribute_map
    exception Attribute_not_found of string
    val find_attribute_id : t list -> string -> int
    val read_from_file_binary : string -> (t, string) Result.t
    val write_to_file_binary : t -> string -> unit
    exception Attribute_type_mismatch of string * string
    val add : t -> Transaction.t -> t
    val delete : t -> Transaction.t -> t
    val query : t list -> query ->  Rule.t list -> result
end
