(* Function - Description
 * idb(s) - returns relations that are heads in stratum s
 * rules(R,s) - returns rules of stratum s with R as head
 * uieval(r) - evaluates all rules in the set r
 * analyze(R) - call to the RDBMS to collect statistics for R
 * dedup(R) - deduplicates R
 *
 * Algorithm 1
 * for each IDB R do
      R <- 0

   // S is a stratification of P
   for each stratum s <= S do
       repeat
          for each R <= idb(s) do
              Rt <- uieval(rules(R,s))
              analyze(Rt)
              Rd <- dedup(Rt)
              analyze(Rs, R)
              dR <- Rs - R
              R <- R + dR
          if s is non-recursive then
              break
       until for all R <= idb(S), dR = 0
 *)

module Make(Value : Sigs.Value) = struct
    module KBE = KnowledgeBase.Make(Value)
    module Types = Value.Types
    module Unify = Unify.Make(Value)
    module Entity = Unify.Entity
    module Sub = Unify.Sub
    module Rule = Unify.Rule
    module Atom = Unify.Atom
    module Index = Index.Make(Value)(Entity)
    module SubGen = Sub_generation.Make(Value)(Unify)(KBE)(Entity)(Index)
    module Func = Rule.Func

    type result = Sub.value Int.Map.t list

    let dump_eavt eavt =
        (*Index.AEVTIndex.to_string_hum eavt (Entity.to_string) |> Caml.print_endline;*)
        Index.AEVTIndex.iter eavt ~f:(fun value ->
            Caml.print_endline Entity.(to_string value)
        )
    ;;

    let string_of_result lst =
        lst
        |> List.map ~f:Sub.to_string
        |> String.concat ~sep:"\n"

    let _string_of_subs_opt : Sub.t option -> string = function
        | None -> "none"
        | Some sub -> Sub.(to_string sub)
    ;;

    module Naive = struct
        type t = {
            rules : Rule.t list;
            p_now : KBE.t;
            d_now : KBE.t;
            p_last : KBE.t;
            d_last : KBE.t;
        }

        let create rules = {
            rules;
            p_now = KBE.empty;
            d_now = KBE.empty;
            p_last = KBE.empty;
            d_last = KBE.empty;
        }
    end

    (* TODO - maybe specializing the query per DB is best? but that would require a reworking
     * of how this whole stratum works... What happens if two DBs have clashing attributes?
     * Could maybe be more explicit about which DB an attribute comes from if ambiguous?
     * dbA.attr instead of just attr.
     *)
    let evaluate_stratum (index : Index.t) (funcs : Func.map) (kb : KBE.t) (structs : Rule.t list list) : KBE.t =
        (* TODO - See if making this core imperative has better performance
         *        it seems folding over the set is slow when there's lots of
         *        elements.
         *)
        (* TODO - Also see if there's a way to do inlining because single body
         *        rules will slow things down
         *)
        (* All rules that make to this are assumed to be recursive *)
        (* Do initial iteration *)
        let structs = List.map structs ~f:(fun rules ->
            let naive = Naive.create rules in
            let d_last = SubGen.eval_rules index funcs kb rules in
            { naive with d_last }
        ) |> Array.of_list in

        let open Naive in

        let len = Array.length structs in
        let calculate_delta_p cur_idx =
            let kb = ref kb in
            for i=0 to len-1 do
                let item = structs.(i) in
                if i = cur_idx then (
                    kb := KBE.merge !kb item.d_last;
                ) else (
                    kb := KBE.merge !kb item.p_last;
                )
            done;
            !kb
        in

        let all_empty () =
            Array.for_all structs ~f:(fun s -> KBE.are_rules_empty s.d_last)
        in

        (* Loop until they're all good *)
        let rec loop () =
            for i=0 to len-1 do
                let naive = structs.(i) in
                if not KBE.(are_rules_empty naive.d_last) then (
                    let delta_p = calculate_delta_p i in
                    let delta_p = SubGen.eval_rules index funcs delta_p naive.rules in
                    let p_now = KBE.merge naive.p_last naive.d_last in
                    let d_now = KBE.diff delta_p p_now in
                    structs.(i) <- { naive with d_now; p_now };
                )
            done;

            (* Swap all the p_now to p_last *)
            for i=0 to len-1 do
                let naive = structs.(i) in
                structs.(i) <- {
                    naive with
                        p_now = KBE.empty;
                        d_now = KBE.empty;
                        p_last = naive.p_now;
                        d_last = naive.d_now;
                }
            done;
            if not (all_empty()) then loop()
        in
        loop ();
        (* Merge all the results *)
        Array.fold structs ~init:kb ~f:(fun kb naive -> KBE.merge kb naive.p_last)
    ;;

    let map_of_rules (rules : Rule.t list) =
        List.fold rules ~init:String.Map.empty ~f:(fun map ((head,_),_  as rule) ->
            match Map.find map head with
            | None -> Map.set map ~key:head ~data:[rule]
            | Some data -> Map.set map ~key:head ~data:(rule :: data)
        )
    ;;

    let semi_naive (index : Index.t) (funcs : Func.map) (kb : KBE.t) (rules : Rule.t list) (components : string list list) : KBE.t =
        (* Gather all rules into a map *)
        let rule_map = map_of_rules rules in

        let get_components comps =
            List.map comps ~f:(fun c -> String.Map.find_exn rule_map c)
        in

        List.fold ~init:kb ~f:(fun kb comps ->
            evaluate_stratum index funcs kb (get_components comps)
        ) components
    ;;

    let extract_query (query : Atom.t) (kb : KBE.t) =
        let name, vars = query in
        let results = List.filter_map ~f:(Unify.unify_terms vars) KBE.(rule_to_list kb name) in
        List.map results ~f:(fun subs ->
            Sub.filter_keys subs ~f:(fun key ->
                List.exists vars ~f:(function
                    | `Var v when Int.(v = key) -> true
                    | _ -> false
                )
            )
        )
    ;;

    module IDC = Graph.Imperative.Digraph.ConcreteLabeled(String)
        (struct
            include Bool
            let default = false
        end)
    module IDCConnected = Graph.Components.Make(IDC)

    let has_negative_cycle (_g : IDC.t) =
        false
    ;;

    let build_precedence_graph (dep_rules : Rule.t list) (rule_set : String.Set.t) =
        (* TODO - Label edges as pos/neg and check if there's a negative cycle *)
        let graph = IDC.create ~size:List.(length dep_rules) () in
        let add_edge from to_ is_neg =
            if String.Set.mem rule_set to_ (* && not simple *) then (
                let edge = IDC.E.create from is_neg to_ in
                IDC.add_edge_e graph edge
            )
        in
        List.iter dep_rules ~f:(fun ((from, _), body) ->
            List.iter body ~f:(fun clause ->
                match clause with
                | Pos, (RuleRef (to_, _)) -> add_edge from to_ false
                | Not, (RuleRef (to_, _)) -> add_edge from to_ true
                | Pos, _ | Not, _ -> ()
            )
        );
        (* Check for negative cycles - per strata? *)
        assert (not (has_negative_cycle graph));
        IDCConnected.scc_list graph
    ;;

    let build_is_simple_map (rules : Rule.t list) =
        let rule_names = Rule.rule_name_set rules in
        List.fold rules ~init:String.Map.empty ~f:(fun map rule ->
            let name = Rule.name rule in
            let is_simple = Rule.is_simple rule_names rule in
            match String.Map.find map name with
            | None -> String.Map.set map ~key:name ~data:is_simple
            | Some v -> String.Map.set map ~key:name ~data:(v && is_simple)
        )
    ;;

    let build_graph (rules : Rule.t list) =
        let simple_rules, dep_rules =
            (*let map = build_is_simple_map rules in*)
            List.partition_tf rules ~f:(fun _rule ->
                false
                (*String.Map.find_exn map Rule.(name rule)*)
                (*|| Rule.has_no_refs rule*) (* <-- 2x slow down? *)
            )
        in
        (* Build dep graph from dep_rules *)
        let comps =
            match rules, build_precedence_graph dep_rules (Rule.rule_name_set dep_rules) with
            | [], comps -> comps
            | rules, [] -> [rules |> Rule.rule_name_set |> String.Set.to_list]
            | _, comps -> comps
        in
        simple_rules, dep_rules, comps
    ;;

    type query = Atom.t * Func.map

    module Transaction = Transaction.Make(Value)
    module Attr = Transaction.Attr
    module Txn = Transaction

    type attribute_map = Attr.t String.Map.t
    [@@deriving sexp, bin_io, compare, equal]

    type t = {
        all_history : Entity.Set.t;
        current_entities : Entity.Set.t;
        attributes : attribute_map;
        index : Index.t;
        next_tid : int;
        next_eid : int;
    }[@@deriving sexp, compare, bin_io]

    let attributes t = t.attributes

    let to_string (t : t) : string =
        let atoms =
            t.current_entities
            |> Entity.Set.to_list
            |> List.map ~f:Entity.to_string
            |> String.concat ~sep:"\n"
        in
        Printf.sprintf "next_eid = %d\nnext_tid = %d\n%s\n"
            t.next_eid
            t.next_tid
            atoms
    ;;

    let time_now() =
        Time_ns.to_int_ns_since_epoch Time_ns.(now())

    exception Attribute_already_defined of string
    exception Duplicate_attribute_in_transaction of string
    let verify_attribute_invariants (strings : attribute_map) (attrs : Txn.Attr.t list) =
        let map = String.Table.create() in
        List.iter attrs ~f:(fun attr ->
            (* Make sure all attributes are unique *)
            if String.Table.mem map attr.name then (
                raise (Duplicate_attribute_in_transaction attr.name)
            ) else (
                String.Table.set map ~key:attr.name ~data:()
            );
            match String.Map.find strings attr.name with
            | None -> ()
            | Some _ -> raise (Attribute_already_defined attr.name)
        );
    ;;

    let attributes_to_entities (t : t) (attrs : Attr.t list) =
        let strings = ref t.attributes in
        let next_eid = ref t.next_eid in
        let entities =
            List.fold ~init:[] ~f:(fun acc attr ->
                let new_strs, e1, e2, e3 = Txn.create_attribute attr !strings !next_eid t.next_tid in
                strings := new_strs;
                incr next_eid;
                e1 :: e2 :: e3 :: acc
            ) attrs
        in
        !strings, !next_eid, entities
    ;;

    type property = Single of Value.t
                  | Multi of Value.t list

    type entity = {
        id : int;
        attributes : property String.Map.t
    }

    let lookup_attribute_name_exn (t : t) (attr_id : int) : string =
        let e_search = Entity.{
            id = attr_id;
            attr = 0;
            value = `Val Value.min_value;
            tid = 0;
            added = true;
        } in
        e_search
        |> Index.EAVTIndex.get_range_start t.index.eavt
        |> Sequence.hd_exn
        |> function
            | {value = `Val value; _} -> Value.symbol_name value
            | _ -> failwith "Lookup attribute failed"
    ;;

    let get_entity (t : t) (id : int) : entity option = 
        let e_search = Entity.{
            id;
            attr = 0;
            value = `Val Value.min_value;
            tid = 0;
            added = true;
        } in
        e_search
        |> Index.EAVTIndex.get_range_start t.index.eavt
        |> Sequence.take_while ~f:(fun (e : Entity.t) -> e.id = id)
        |> Sequence.to_list
        |> function
            | [] -> None
            | values ->
                let attributes : property String.Map.t = List.fold 
                    values ~init:String.Map.empty ~f:(fun map ent ->
                        let attr_name = lookup_attribute_name_exn t ent.attr in
                        let value =
                            let e_val = match ent.value with
                                      | `ERef id -> Value.of_int id
                                      | `Val v -> v
                            in
                            match String.Map.find map attr_name with
                            | None -> Single e_val
                            | Some (Multi v) -> Multi (e_val :: v)
                            | Some (Single v) -> Multi [v; e_val]
                        in
                        String.Map.set map ~key:attr_name ~data:value
                    )
                in
                Some { id; attributes }
    ;;

    let datom_id_to_int start_eid = function
        | Transaction.Abs id -> id
        | Rel id -> start_eid + id
    ;;

    let datom_value_to_value start_eid = function
        | Transaction.Ref (Rel id) -> `ERef (start_eid + id)
        | Ref (Abs id) -> `ERef id
        | Val v -> `Val v
    ;;

    let build_transaction_add (t : t) (txn : Transaction.t) =
        (* No duplicates or existing attributes allowed *)
        verify_attribute_invariants t.attributes txn.attributes; 
        (* Although user can still manually mess things up with
         * the datum_full function, maybe put that in an "Expert" module
         *)

        (* At this point all attributes don't exist in the DB *)
        let strings, next_eid, attrs = attributes_to_entities t txn.attributes in

        (* Tricky part, now we want to add the datoms in reverse order of what
         * they were specified in. This way we can do one pass to check if they
         * need to be marked as "delete" because that entity already existed.
         * An add on an attribute that is "one" is an update/replace.
         *)
        (*let card_map = build_attribute_cardinality_map strings t.index.eavt attrs txn.datoms in*)

        let get_attr_card attr_name =
            let attr = String.Map.find_exn strings attr_name in
            attr.eid, attr.cardinality
        in

        (* Build the transaction datoms now *)
        let txn_datoms = List.map txn.txn_attrs ~f:(fun (attr, value) ->
            Transaction.{
                eid = Abs t.next_tid;
                attr = attr;
                value = value;
                tid = Some t.next_tid;
            }
        ) in

        let datoms = List.rev txn.datoms @ txn_datoms in

        (* Loop through the datoms, and build the appropriate add/delete values *)
        let delete_set = ref Entity.Set.empty in
        let add_set = ref Entity.Set.(of_list attrs) in
        let rolling_eavt = ref t.index.eavt in
        List.iter datoms ~f:(fun datom ->
            (* Check cardinality of the attribute *)
            let attr, card = get_attr_card datom.attr in
            let e =
                Entity.{
                    id = datom_id_to_int next_eid datom.eid;
                    attr;
                    value = datom_value_to_value next_eid datom.value;
                    tid = t.next_tid;
                    added = true;
                } 
            in
            add_set := Set.add !add_set e; (* No matter what we'll add this datom *)
            begin match card with
            | Many -> (* Easy, just add it! *) ()
            | One -> (* Ok, check if it already has a value... *)
                let e_search = { e with tid = 0; value = `Val Value.min_value } in
                let seq = Index.EAVTIndex.get_range_start !rolling_eavt e_search in
                match Sequence.hd seq with
                | None -> ()
                | Some hd -> 
                    (* If these values match up to the tid, then we must delete it *)
                    (*Printf.printf "%s ?? %s\n\n" Entity.(to_string hd) Entity.(to_string e);*)
                    if hd.id = e.id && hd.attr = e.attr then (
                        delete_set := Entity.Set.add !delete_set hd
                    );
            end;
            rolling_eavt := Index.EAVTIndex.add !rolling_eavt e
        );

        let next_eid = next_eid + txn.next_eid in
        let txn =
            let timestamp =
                `Val (Value.of_time (
                    if txn.deterministic_txn_timestamp then 0 else (time_now())
                ))
            in
            Entity.{
            id = next_eid;
            attr = String.Map.(find_exn strings "db/transaction").eid;
            value = timestamp;
            tid = t.next_tid;
            added = true;
        } in

        add_set := Entity.Set.add !add_set txn;

        { t with attributes=strings; next_eid=next_eid+1; next_tid=t.next_tid+1 }, !add_set, !delete_set
    ;;

    let build_transaction (t : t) (entities : Transaction.t) added =
        let strings = ref t.attributes in
        let next_eid = ref t.next_eid in

        (* Build all the attributes first *)
        let create_attr (attr : Attr.t) =
            let new_strs , e1, e2, e3 = Txn.create_attribute attr !strings !next_eid t.next_tid in
            strings := new_strs;
            incr next_eid;
            (e1, e2, e3)
        in

        let datoms = List.fold ~init:[] ~f:(fun lst attr ->
            let e1, e2, e3 = create_attr attr in
            e1 :: e2 :: e3 :: lst
        ) entities.attributes in

        (* Need to build a table of id -> id *)
        let start_eid = !next_eid in
        let datom_value_to_value = function
            | Transaction.Ref (Rel id) -> `ERef (start_eid + id)
            | Ref (Abs id) -> `ERef id
            | Val v -> `Val v
        in

        let datom_id_to_int = function
            | Transaction.Abs id -> id
            | Rel id -> start_eid + id
        in

        let datom_tid_to_int = function
            | Some id -> id
            | None -> t.next_tid
        in

        let create_datom (datom : Transaction.datom) =
            (* If we're adding we may have to create a deletion list as well... *)
            let e1 = Entity.{
                id = datom_id_to_int datom.eid;
                attr = String.Map.(find_exn !strings datom.attr).eid;
                value = datom_value_to_value datom.value;
                tid = datom_tid_to_int datom.tid;
                added;
            } in
            e1
        in

        (* TODO - check cardinality?, need deletion datoms *)

        let datoms = List.fold ~init:datoms ~f:(fun lst datom -> create_datom datom :: lst) entities.datoms in
        next_eid := !next_eid + entities.next_eid;

        (* Create extra datoms for the transaction *)
        let txn_datoms = List.map entities.txn_attrs ~f:(fun (attr, value) ->
            Entity.{
                id = t.next_tid;
                attr = String.Map.(find_exn !strings attr).eid;
                value = datom_value_to_value value;
                tid = t.next_tid;
                added = true;
            }
        ) in

        let txn =
            let timestamp =
                `Val (Value.of_time (
                    if entities.deterministic_txn_timestamp then 0 else (time_now())
                ))
            in
            Entity.{
            id = !next_eid;
            attr = String.Map.(find_exn !strings "db/transaction").eid;
            value = timestamp;
            tid = t.next_tid;
            added = true;
        } in
        { t with
            next_eid = !next_eid+1;
            next_tid = t.next_tid+1;
            attributes = !strings }, (txn :: txn_datoms @ datoms)
    ;;

    exception Attribute_type_mismatch of string * string
    let validate_transaction (attributes : attribute_map) ~(datoms : Txn.datom list) =
        List.iter datoms ~f:(fun datom ->
            let check_type t v =
                match Value.Types.(is_valid t v) with
                | false -> raise (Attribute_type_mismatch (datom.attr, Value.to_string v))
                | true -> ()
            in
            let attr = String.Map.find_exn attributes datom.attr in
            match datom.value with
            | Val dv -> check_type attr.type_ dv
            | Ref (Abs id) -> check_type attr.type_ Value.(of_int id)
            | Ref (Rel id) -> check_type attr.type_ Value.(of_int id)
        );
    ;;

    let add (t : t) (txn : Transaction.t) =
        let t, add_set, delete_set = build_transaction_add t txn in
        validate_transaction t.attributes ~datoms:txn.datoms;

        let all_history =
            let delete_set = Entity.Set.map delete_set ~f:(fun e -> {e with added = false}) in
            Entity.Set.union t.all_history add_set |> Entity.Set.union delete_set
        in
        let index = Index.insert t.index add_set in
        let index = Index.remove index delete_set in
        (*if Entity.Set.is_empty delete_set |> not then (
            Printf.printf "\n\n RESULT \n\n";
            dump_eavt index.aevt;
            Entity.Set.iter add_set ~f:(fun e ->
                Printf.printf "ADDING ----- %s\n" Entity.(to_string e);
            );
            Entity.Set.iter delete_set ~f:(fun e ->
                Printf.printf "DELETING ----- %s\n" Entity.(to_string e);
            );
        );*)
        { t with
            all_history;
            index;
            current_entities =
                Entity.Set.union
                    (Entity.Set.diff t.current_entities delete_set)
                    add_set
        }
        (*|> (fun t -> dump_eavt t.index.aevt; t)*)
    ;;

    let delete (t : t) (entities : Transaction.t) =
        let t, datoms = build_transaction t entities false in
        let history_set = Entity.Set.of_list datoms in
        let txn, rest = match datoms with
                      | [] -> failwith "Expected not empty"
                      | hd :: tl -> hd, tl
        in
        (*List.iter rest ~f:(fun e ->
            Caml.print_endline Entity.(to_string e);
        );*)
        let rest = List.map rest ~f:(fun e -> { e with added = true }) |> Entity.Set.of_list in
        let index = Index.remove t.index rest in
        (*dump_eavt index.aevt;*)
        { t with
            all_history = Entity.Set.union t.all_history history_set;
            index;
            current_entities =
                Entity.Set.add
                    (Entity.Set.diff t.current_entities rest)
                    txn
                ;
        }
    ;;

    let empty =
        let base_db = {
            all_history = Entity.Set.empty;
            current_entities = Entity.Set.empty;
            attributes = String.Map.empty;
            index = Index.empty;
            next_tid = 1;
            next_eid = 6;
        } in
        (* Add internal DB information *)
        let ident_id = 0 in
        let card_id = 1 in
        let value_id = 2 in
        let doc_id = 3 in
        let txn_id = 4 in
        let mk id attr value = Entity.{
            id; attr; value; tid = 0; added = true;
        } in

        let cardinality = Attr.One in
        let attrs : Attr.t list = [
            { eid = ident_id; name = "db/attribute";   type_ = Value.Types.db_symbol; cardinality; };
            { eid = card_id;  name = "db/cardinality"; type_ = Value.Types.db_cardinality; cardinality; };
            { eid = value_id; name = "db/valueType";   type_ = Value.Types.db_type; cardinality; };
            { eid = doc_id;   name = "db/doc";         type_ = Value.Types.db_string; cardinality; };
            { eid = txn_id;   name = "db/transaction"; type_ = Value.Types.db_timestamp; cardinality };
        ] in

        let attribute_map = List.fold ~init:String.Map.empty ~f:(fun map (attr : Attr.t) ->
            String.Map.add_exn map ~key:attr.name ~data:attr
        ) attrs in

        let lookup name = String.Map.(find_exn attribute_map name).eid in

        let sym_val v = `Val (Value.of_symbol v) in
        let val_type v = `Val (Value.Types.to_value v) in

        let entities = List.fold attrs ~init:[] ~f:(fun lst attr ->
            let name  = mk attr.eid (lookup "db/attribute") (sym_val attr.name) in
            let card  = mk attr.eid (lookup "db/cardinality") (`Val Value.cardinal_one) in
            let value = mk attr.eid (lookup "db/valueType") (val_type attr.type_) in
            name :: card :: value :: lst
        ) in

        let txn = mk 5 txn_id (`Val (Value.of_time 0)) in

        let entities = Entity.Set.of_list (txn :: entities) in

        let db =
            { base_db with 
                index = Index.insert base_db.index entities; 
                attributes = attribute_map;
            }
        in

        (* Lets add a couple more "extras" on top of the base attributes *)
        let open Transaction in
        let str_val s = Val (Value.of_string s) in
        let ed = empty_deterministic in
        let ed = datom_full ed (eid_of_int ident_id) "db/doc" (str_val "Attribute name.") 1 in
        let ed = datom_full ed (eid_of_int card_id) "db/doc" (str_val "Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity.") 1 in
        let ed = datom_full ed (eid_of_int value_id) "db/doc" (str_val "The type of value the attribute will hold.") 1 in
        let ed = datom_full ed (eid_of_int doc_id) "db/doc" (str_val "Documentation about the attribute.") 1 in
        let ed = datom_full ed (eid_of_int txn_id) "db/doc" (str_val "A transaction timestamp.") 1 in
        add db ed
    ;;

    let write_to_file_binary (t : t) file =
        let size = bin_size_t t in
        Printf.printf "Size %d\n" size;
        Out_channel.with_file
            ~binary:true
            ~fail_if_exists:false
            ~append:false
            file
            ~f:(fun chan ->
                let buf = Bin_prot.Utils.bin_dump bin_writer_t t in
                for i=0 to Bigarray.Array1.(dim buf - 1) do
                    Out_channel.output_char chan Bigarray.Array1.(get buf i)
                done
            );
        (*let fd = Unix.openfile ~mode:[O_WRONLY; O_CREAT; O_TRUNC] file in
        let bs = Core.Bigstring.create size in
        bin_write_t ~pos:0 bs t |> ignore;
        (*Unix.ftruncate fd ~len:Bigarray.Array1.(dim buf |> Int64.of_int);*)
        Unix.close fd
        (*Core.Bigstring.write fd bs |> ignore;*)
        *)
    ;;

    let file_size fd =
        let pos = Unix.lseek fd Int64.zero ~mode:SEEK_CUR in
        let bytes = Unix.lseek fd Int64.zero ~mode:SEEK_END in
        let _ = Unix.lseek fd pos ~mode:SEEK_SET in
        bytes
    ;;

    let read_from_file_binary file =
        (*try*)
            Printf.printf "File %s\n" file;
            let fd = Unix.openfile ~mode:[O_RDONLY] file in
            let size = file_size fd in
            let mmap = Unix.map_file ~shared:false fd Bigarray.char Bigarray.c_layout [|Int64.to_int_exn size|] in
            let mmap = Bigarray.array1_of_genarray mmap in
            Printf.printf "Size %d - Arr %d\n" Int64.(to_int_exn size) Bigarray.Array1.(dim mmap);
            let pos_ref = ref 0 in
            let t = bin_read_t mmap ~pos_ref in
            Unix.close fd;
            Printf.printf "Pos %d\n" !pos_ref;
            Ok t
            (*
        with e ->
            Caml.print_endline Exn.(to_string e);
            Error `Failed_to_load_db
            *)
    ;;

    type db_stats = {
        interned_strings : attribute_map;
        eavt_size : int;
    }

    let get_stats (t : t) = {
        interned_strings = t.attributes;
        eavt_size = Index.EAVTIndex.length t.index.eavt;
    }

    exception Attribute_not_found of string
    let find_attribute_id (dbs : t list) (attr_name : string) : int =
        match dbs with
        | [] -> raise (Attribute_not_found attr_name)
        | [db] -> String.Map.(find_exn db.attributes attr_name).eid
        | lst ->
            let offset = ref 0 in
            List.find_map lst ~f:(fun db ->
                match String.Map.find db.attributes attr_name with
                | None ->
                    offset := !offset + db.next_eid;
                    None
                | Some attr -> Some (attr.eid + !offset)
            ) |> function
                | None -> raise (Attribute_not_found attr_name)
                | Some id -> id
    ;;

    let merge_dbs (dbs : t list) : Index.t =
        match dbs with
        | [] -> Index.empty
        | [db] -> db.index
        | hd :: tl ->
            let offset = hd.next_eid in
            List.fold tl ~init:(offset, hd.index) ~f:(fun (offset, index) db ->
                let index =
                    Index.merge_with_fn index db.index ~f:(Entity.offset_by ~offset)
                in
                (db.next_eid + offset, index)
            ) |> snd
    ;;

    (* TODO - pass in Func.t IntMap.t as well *)
    let query (t : t list) ((query, funcs) : query) (rules : Rule.t list) : result =
        SubGen.iters := 0;
        (*Caml.print_endline (StringIntern.to_string t.strings);*)
        let rules = List.map ~f:Rule.reorder_clauses rules in
        (*Printf.printf "QUERY: %s\n" Atom.(to_string query);*)
        List.iter rules ~f:(fun r ->
            Caml.print_endline Rule.(to_string r)
        );
        assert List.(for_all rules ~f:Rule.is_range_restricted);
        (*
        *)
        (* Build graph *)
        let simple_rules, dep_rules, comps = build_graph rules in
        (* We only need to eval simple rules once *)
        let kb = KBE.empty in
        (*Printf.printf "EAVTIndex size %d\n" Index.EAVTIndex.(length t.index.eavt);*)
        (*Caml.print_endline KBE.(to_string kb);*)
        (*
        Printf.printf "Simple rules length %d\n" (List.length simple_rules);
        Printf.printf "Dep rules length %d\n" (List.length dep_rules);
        Printf.printf "Total Rules %d\n" (List.length rules);
        *)
        (* TODO - might need to do something smarter with these "simple" queries
         *        maybe remove their results after the first iteration. They slow
         *        down larger queries because their EDBs stay around for all the
         *        later iterations when they're not needed anymore
         *)
        (* Merge all the indices together so we can query across multiple dbs *)
        let index = merge_dbs t in
        (*List.iter ~f:(fun t ->
            Printf.printf "DB\n";
            dump_eavt t.index.aevt) t;*)
        (*dump_eavt index.aevt;*)
        let kb = KBE.merge (SubGen.eval_rules index funcs kb simple_rules) kb in
        (*Caml.print_endline KBE.(to_string kb);*)

        (*
        Caml.print_endline "Components";
        List.iter comps ~f:(fun lst ->
            let lst = String.concat lst ~sep:" " in
            Printf.printf "[%s]\n" lst
        );
        *)

        let kb =
            (*
            timeit "semi_naive" (fun () ->
            )
            *)
            semi_naive index funcs kb dep_rules comps
        in
        (*Printf.printf "Iters %d\n" !iters;
           timeit "extract" (fun () ->
           )
           *)
       extract_query query kb
    ;;
end
