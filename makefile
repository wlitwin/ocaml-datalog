.PHONY: all clean run test release repl perf promote report perf-run

all:
	dune build bin/main.exe

release:
	dune build bin/main.exe --profile release

clean:
	dune clean

test:
	dune runtest 

promote:
	dune runtest --auto-promote

run: all
	_build/default/bin/main.exe

repl: all
	@rlwrap _build/default/bin/main.exe

report:
	sudo perf report

perf-run:
	sudo perf record --call-graph=dwarf -- _build/default/bin/main.exe

update:
	opam update ocaml_datalog && opam upgrade ocaml_datalog

perf: perf-run report
