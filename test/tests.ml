open Ocaml_datalog_edn
module Db = Edn_db
open Db

(*let* x = e1 in e2 x
 * desugars to the equivalent of:
 * e1 >>= fun x -> e2 x
 *)
let rule head vars body = 
    ((head, vars), body)

let pat e a v =
    Rule.Pos, (Rule.Clause.Atom (e, a, v, `Ignore))

let rref name pats =
    Rule.Pos, (Rule.Clause.RuleRef (name, pats))

let not = function
    | Rule.Pos, a -> Rule.Not, a
    | Rule.Not, _ as n -> n

let link_db = 
    let open Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let id3, ed = make_id ed in
    let id4, ed = make_id ed in
    let ed = attribute ed "edge" Attr.One Types.db_ref in
    let ed = datom ed id1 "edge" (Ref id2) in
    let ed = datom ed id2 "edge" (Ref id3) in
    let ed = datom ed id3 "edge" (Ref id3) in
    let ed = datom ed id3 "edge" (Ref id4) in
    (*let ed = datom (txn_id ed) "txn/description" (Val (`String "Adding some base things")) in*)
    Db.add Db.empty ed
;;

let link_db1, link_db2 = 
    let open Transaction in
    let link_db1 =
        let ed = empty_deterministic in
        let id1, ed = make_id ed in
        let id2, ed = make_id ed in
        let id3, ed = make_id ed in
        let ed = attribute ed "value" Attr.One Value.Types.db_int in
        let ed = attribute ed "name" Attr.One Value.Types.db_string in
        let ed = datom ed id1 "name" (Val (`String "name1")) in
        let ed = datom ed id2 "name" (Val (`String "name2")) in
        let ed = datom ed id3 "name" (Val (`String "name3")) in
        let ed = datom ed id1 "value" (Val (`Int 10)) in
        let ed = datom ed id2 "value" (Val (`Int 20)) in
        let ed = datom ed id3 "value" (Val (`Int 30)) in
        (*let tx = datom (txn_id ed) "txn/description" (Val (`String "Adding some base things")) in*)
        Db.add Db.empty ed
    in
    let link_db2 =
        let ed = empty_deterministic in
        let id1, ed = make_id ed in
        let id2, ed = make_id ed in
        let id3, ed = make_id ed in
        let ed = attribute ed "other_name" Attr.One Value.Types.db_string in
        let ed = datom ed id1 "other_name" (Val (`String "name1")) in
        let ed = datom ed id2 "other_name" (Val (`String "name2")) in
        let ed = datom ed id3 "other_name" (Val (`String "name4")) in
        (*let tx = datom (txn_id ed) "txn/description" (Val (`String "Adding some base things 2")) in*)
        Db.add Db.empty ed
    in
    link_db1, link_db2
;;

let across_link_db_query = {|
    [:find [x y]
     :where
     [id :name x]
     [_ :other_name x]
     [id :value y]
    ]
|}

let unreachable_query = {|
    [:find [x y]
     :where
     (unreachable x y)
     :rules
     [(reachable x y)
      [x :edge y]]
     [(reachable x y)
      [x :edge z]
      (reachable z y)]
     [(node x)
      [x :edge _]]
     [(node y)
      [_ :edge y]]
     [(unreachable x y)
      (node x)
      (node y)
      (not (reachable x y))]
    ]
|}

let link_query = {|
    [:find [x y]
     :where
     (reachable x y)
     :rules
     [(reachable x y)
      [x :edge y]]
     [(reachable x y)
      [x :edge z]
     (reachable z y)]
    ]
|}

let reachable_query = {|
    [:find [x y]
     :where (reachable x y)
     :rules
     [(reachable x y)
      [x :edge y]
     ]
     [(reachable x y)
      [x :edge z]
      (reachable z y)
     ]
    ]
|}

(*
let link_query = {|
    [:find [y]
     :where [_ :edge y]
    ]
|}
*)

(*

let lots_of_advisors =
    let prog = [
        e 0 "advisor" (`ERef 1);
        e 1 "advisor" (`ERef 2);
        e 2 "advisor" (`ERef 3);
    ] in
    let rec loop idx acc =
        if idx <= 0 then acc
        else (
            let i = idx + 40 in
            let i' = (idx + 40) -1 in
            loop (idx - 1) ((e i "advisor" (`ERef i')) :: acc)
        )
    in
    let atoms = loop 400 prog in
    atoms, [
        rule "ancestor" [`Var "X"; `Var "Y"] [
            pat (`Var "X") (`Sym "advisor") (`Var "Y");
        ];

        rule "ancestor" [`Var "A"; `Var "B"] [
            pat (`Var "A") (`Sym "advisor") (`Var "C");
            rref "ancestor" [`Var "C"; `Var "B"];
        ];
    ]
;;

let string_of_sub_list lst =
    lst
    |> List.map ~f:Sub.to_string
    |> String.concat ~sep:"\n"

    (*

let run() =
    (*do_query ("reachable", [`Var "X"; `Var "Y"]) link*)
    (*do_query ("unreachable", [`Var "X"; `Var "Y"]) not_reachable*)
    do_query ("ancestor", [`Var "X"; `Var "Y"]) lots_of_advisors
;;
*)
    *)

let cond_db = 
    let open Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let id3, ed = make_id ed in
    let id4, ed = make_id ed in
    let id5, ed = make_id ed in
    let ed = attribute ed "desc" Attr.Many Types.db_user in
    let ed = datom ed id1 "desc" (Val (`String "cool")) in
    let ed = datom ed id2 "desc" (Val (`String "awesome")) in
    let ed = datom ed id3 "desc" (Val (`Int 20)) in
    let ed = datom ed id4 "desc" (Val (`Int 40)) in
    let ed = datom ed id5 "desc" (Val (`Int 40)) in
    (*let tx = datom (txn_id ed) "txn/description" (Val (`String "Adding some base things")) in*)
    Db.add Db.empty ed
;;

let cond_query = {|
    [:find [x] :where (= v 40) [x :desc v]]
|}

(* What should this include?
 *
 * = awesome && desc || v < 40 && desc
 *
 * not (!= awesome || !desc && v >= 40 && !desc)
 *)
let cond_query2 = {|
    [:find [x y]
     :where [x _ y] (not (criteria x))
     :rules
     [(criteria x)
      (= v "awesome")
      [x :desc v]
     ]
     [(criteria x)
      (not (> v 39))
      [x :desc v]
     ]
    ]
|}

(*
     *)
let cond_query3 = {|
    [:find [x y z t] :where [x y z t]]
|}

let cond_query4 = {|
    [:find [x y z t] 
     :where (find_all x y z t) 
     :rules
     [(find_all x y z t) [x y z t]]
    ]
|}

let run_query db q p =
    let res = Db.query db q p in
    Caml.print_endline (Db.string_of_result res);
    (*Caml.print_endline (Db.to_string db);*)
    Printf.printf "Number of results %d\n" List.(length res);
;;

let big_query () =
    let open Transaction in
    let rec loop prefix ed ext count =
        if count > 0 then (
            let id1, ed = make_id ed in
            let ed = datom ed id1 "file/ext" (Val (`String ext)) in
            let ed = datom ed id1 "file/name" (Val (`String (prefix ^ Int.(to_string count)))) in
            loop prefix ed ext (count-1);
        ) else ed
    in
    (* Total DB count should be 20001, 2x10000 + 1xTransaction *)
    let ed = attribute empty_deterministic "file/ext" Attr.One Types.db_user in
    let ed = attribute ed "file/name" Attr.One Types.db_user in
    let ed = loop "file" ed "bin" 100 in
    let ed = loop "executable" ed "exe" 100 in
    Db.add Db.empty ed
;;

(*
let make_db () =
    let ed = use Db.empty in
    ed 
    |> edn "file/ext" (`String "ok")
    |> edn "file/ext" (`String "ok")
    |> fun id -> edn_id id "file/ext" (`String "cool")
    |> edn_id "file/name" (`String "Ok")
    |> 
;;
*)

let file_query = {|
    [:find [x]
     :where 
     [e :file/ext "bin"]
     [e :file/name x]
    ]
|}

let file_query_indirect = {|
    [:find [x]
     :where 
     (bin_files x "bin")
     :rules
     [(bin_files x y)
         [e :file/ext y]
         [e :file/name x]
     ]
    ]
|}

let file_query_not = {|
    [:find [x]
     :where
     (not [e :file/ext "bin"])
     [e :file/name x]
    ]
|}

let simple = {|
    [:find [x y]
     :where [x :edge y]
    ]
|}

let do_query db query =
    match Db.query_from_string db query with
    | Error e -> failwith e
    | Ok (q, r, _) -> run_query db q r
;;

let simple_cond_db =
    let open Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let ed = attribute ed "file/name" Attr.Many Value.Types.db_string in
    let ed = datom ed id1 "file/name" (Val (`String "cool")) in
    let ed = datom ed id2 "file/name" (Val (`String "awesome")) in
    Db.add Db.empty ed
;;

let%expect_test "simple_cond" =
    do_query [simple_cond_db] {|
        [:find [name] 
         :where (find_eq name)
         :rules
         [(find_eq name)
             [x :file/name name] 
             (= name "cool")
         ]
        ]
    |};
    [%expect {|
      find_eq(?0)
        +(Atom (`Var (1), `Int (7), `Var (0), `Ignore))
        +(Cond (Eq, `Var (0), `Val (`String ("cool"))))
      [0=>"cool"]
      Number of results 1 |}]
;;

let%expect_test _ =
    do_query [link_db1; link_db2] across_link_db_query;
    [%expect {|
      query_rule(?0, ?1)
        +(Atom (`Var (2), `Int (7), `Var (0), `Ignore))
        +(Atom (`Var (2), `Int (8), `Var (1), `Ignore))
        +(Atom (`Ignore, `Int (20), `Var (0), `Ignore))
      [0=>"name1"; 1=>10]
      [0=>"name2"; 1=>20]
      Number of results 2 |}]
;;

let%expect_test _ =
    do_query [link_db] simple;
    [%expect {|
      query_rule(?0, ?1)
        +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
      [0=>8; 1=>9]
      [0=>9; 1=>10]
      [0=>10; 1=>10]
      [0=>10; 1=>11]
      Number of results 4 |}]
;;

let%expect_test _ =
    do_query [link_db] unreachable_query;
    [%expect {|
      unreachable(?0, ?1)
        +(RuleRef ("node", [`Var (0)]))
        +(RuleRef ("node", [`Var (1)]))
        -(RuleRef ("reachable", [`Var (0); `Var (1)]))
      node(?1)
        +(Atom (`Ignore, `Int (7), `Var (1), `Ignore))
      node(?0)
        +(Atom (`Var (0), `Int (7), `Ignore, `Ignore))
      reachable(?0, ?1)
        +(RuleRef ("reachable", [`Var (2); `Var (1)]))
        +(Atom (`Var (0), `Int (7), `Var (2), `Ignore))
      reachable(?0, ?1)
        +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
      [0=>8; 1=>8]
      [0=>9; 1=>8]
      [0=>9; 1=>9]
      [0=>10; 1=>8]
      [0=>10; 1=>9]
      [0=>11; 1=>8]
      [0=>11; 1=>9]
      [0=>11; 1=>10]
      [0=>11; 1=>11]
      Number of results 9 |}]
;;

let%expect_test _ =
    do_query [link_db] reachable_query;
    [%expect {|
      reachable(?0, ?1)
        +(RuleRef ("reachable", [`Var (2); `Var (1)]))
        +(Atom (`Var (0), `Int (7), `Var (2), `Ignore))
      reachable(?0, ?1)
        +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
      [0=>8; 1=>9]
      [0=>8; 1=>10]
      [0=>8; 1=>11]
      [0=>9; 1=>10]
      [0=>9; 1=>11]
      [0=>10; 1=>10]
      [0=>10; 1=>11]
      Number of results 7 |}]
;;

let%expect_test _ =
    do_query [cond_db] cond_query;
    [%expect {|
      query_rule(?0)
        +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
        +(Cond (Eq, `Var (1), `Val (`Int (40))))
      [0=>11]
      [0=>12]
      Number of results 2 |}]
;;

let%expect_test _ =
    do_query [cond_db] cond_query2;
    [%expect {|
      query_rule(?0, ?1)
        +(Atom (`Var (0), `Ignore, `Var (1), `Ignore))
        -(RuleRef ("criteria", [`Var (0)]))
      criteria(?0)
        +(Atom (`Var (0), `Int (7), `Var (2), `Ignore))
        -(Cond (Gt, `Var (2), `Val (`Int (39))))
      criteria(?0)
        +(Atom (`Var (0), `Int (7), `Var (2), `Ignore))
        +(Cond (Eq, `Var (2), `Val (`String ("awesome"))))
      [0=>0; 1=>"Attribute name."]
      [0=>0; 1=>"db/attribute"]
      [0=>0; 1=>"db/symbol"]
      [0=>0; 1=>"one"]
      [0=>1; 1=>"Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity."]
      [0=>1; 1=>"db/cardinality"]
      [0=>1; 1=>"one"]
      [0=>2; 1=>"The type of value the attribute will hold."]
      [0=>2; 1=>"db/type"]
      [0=>2; 1=>"db/valueType"]
      [0=>2; 1=>"one"]
      [0=>3; 1=>"Documentation about the attribute."]
      [0=>3; 1=>"db/doc"]
      [0=>3; 1=>"db/string"]
      [0=>3; 1=>"one"]
      [0=>4; 1=>"A transaction timestamp."]
      [0=>4; 1=>"db/timestamp"]
      [0=>4; 1=>"db/transaction"]
      [0=>4; 1=>"one"]
      [0=>5; 1=>0]
      [0=>6; 1=>0]
      [0=>7; 1=>"db/user"]
      [0=>7; 1=>"desc"]
      [0=>7; 1=>"many"]
      [0=>11; 1=>40]
      [0=>12; 1=>40]
      [0=>13; 1=>0]
      Number of results 27 |}]
;;

let%expect_test _ =
    do_query [cond_db] cond_query3;
    [%expect {|
      query_rule(?0, ?1, ?2, ?3)
        +(Atom (`Var (0), `Var (1), `Var (2), `Var (3)))
      [0=>0; 1=>0; 2=>"db/attribute"; 3=>0]
      [0=>0; 1=>1; 2=>"one"; 3=>0]
      [0=>0; 1=>2; 2=>"db/symbol"; 3=>0]
      [0=>0; 1=>3; 2=>"Attribute name."; 3=>1]
      [0=>1; 1=>0; 2=>"db/cardinality"; 3=>0]
      [0=>1; 1=>1; 2=>"one"; 3=>0]
      [0=>1; 1=>2; 2=>"db/cardinality"; 3=>0]
      [0=>1; 1=>3; 2=>"Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity."; 3=>1]
      [0=>2; 1=>0; 2=>"db/valueType"; 3=>0]
      [0=>2; 1=>1; 2=>"one"; 3=>0]
      [0=>2; 1=>2; 2=>"db/type"; 3=>0]
      [0=>2; 1=>3; 2=>"The type of value the attribute will hold."; 3=>1]
      [0=>3; 1=>0; 2=>"db/doc"; 3=>0]
      [0=>3; 1=>1; 2=>"one"; 3=>0]
      [0=>3; 1=>2; 2=>"db/string"; 3=>0]
      [0=>3; 1=>3; 2=>"Documentation about the attribute."; 3=>1]
      [0=>4; 1=>0; 2=>"db/transaction"; 3=>0]
      [0=>4; 1=>1; 2=>"one"; 3=>0]
      [0=>4; 1=>2; 2=>"db/timestamp"; 3=>0]
      [0=>4; 1=>3; 2=>"A transaction timestamp."; 3=>1]
      [0=>5; 1=>4; 2=>0; 3=>0]
      [0=>6; 1=>4; 2=>0; 3=>1]
      [0=>7; 1=>0; 2=>"desc"; 3=>2]
      [0=>7; 1=>1; 2=>"many"; 3=>2]
      [0=>7; 1=>2; 2=>"db/user"; 3=>2]
      [0=>8; 1=>7; 2=>"cool"; 3=>2]
      [0=>9; 1=>7; 2=>"awesome"; 3=>2]
      [0=>10; 1=>7; 2=>20; 3=>2]
      [0=>11; 1=>7; 2=>40; 3=>2]
      [0=>12; 1=>7; 2=>40; 3=>2]
      [0=>13; 1=>4; 2=>0; 3=>2]
      Number of results 31 |}]
;;

let%expect_test _ =
    do_query [cond_db] cond_query4;
    [%expect {|
      find_all(?0, ?1, ?2, ?3)
        +(Atom (`Var (0), `Var (1), `Var (2), `Var (3)))
      [0=>0; 1=>0; 2=>"db/attribute"; 3=>0]
      [0=>0; 1=>1; 2=>"one"; 3=>0]
      [0=>0; 1=>2; 2=>"db/symbol"; 3=>0]
      [0=>0; 1=>3; 2=>"Attribute name."; 3=>1]
      [0=>1; 1=>0; 2=>"db/cardinality"; 3=>0]
      [0=>1; 1=>1; 2=>"one"; 3=>0]
      [0=>1; 1=>2; 2=>"db/cardinality"; 3=>0]
      [0=>1; 1=>3; 2=>"Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity."; 3=>1]
      [0=>2; 1=>0; 2=>"db/valueType"; 3=>0]
      [0=>2; 1=>1; 2=>"one"; 3=>0]
      [0=>2; 1=>2; 2=>"db/type"; 3=>0]
      [0=>2; 1=>3; 2=>"The type of value the attribute will hold."; 3=>1]
      [0=>3; 1=>0; 2=>"db/doc"; 3=>0]
      [0=>3; 1=>1; 2=>"one"; 3=>0]
      [0=>3; 1=>2; 2=>"db/string"; 3=>0]
      [0=>3; 1=>3; 2=>"Documentation about the attribute."; 3=>1]
      [0=>4; 1=>0; 2=>"db/transaction"; 3=>0]
      [0=>4; 1=>1; 2=>"one"; 3=>0]
      [0=>4; 1=>2; 2=>"db/timestamp"; 3=>0]
      [0=>4; 1=>3; 2=>"A transaction timestamp."; 3=>1]
      [0=>5; 1=>4; 2=>0; 3=>0]
      [0=>6; 1=>4; 2=>0; 3=>1]
      [0=>7; 1=>0; 2=>"desc"; 3=>2]
      [0=>7; 1=>1; 2=>"many"; 3=>2]
      [0=>7; 1=>2; 2=>"db/user"; 3=>2]
      [0=>8; 1=>7; 2=>"cool"; 3=>2]
      [0=>9; 1=>7; 2=>"awesome"; 3=>2]
      [0=>10; 1=>7; 2=>20; 3=>2]
      [0=>11; 1=>7; 2=>40; 3=>2]
      [0=>12; 1=>7; 2=>40; 3=>2]
      [0=>13; 1=>4; 2=>0; 3=>2]
      Number of results 31 |}]
;;

let%expect_test _ =
    do_query [big_query()] file_query;
    [%expect {|
      query_rule(?0)
        +(Atom (`Var (1), `Int (8), `Val (`String ("bin")), `Ignore))
        +(Atom (`Var (1), `Int (7), `Var (0), `Ignore))
      [0=>"file1"]
      [0=>"file10"]
      [0=>"file100"]
      [0=>"file11"]
      [0=>"file12"]
      [0=>"file13"]
      [0=>"file14"]
      [0=>"file15"]
      [0=>"file16"]
      [0=>"file17"]
      [0=>"file18"]
      [0=>"file19"]
      [0=>"file2"]
      [0=>"file20"]
      [0=>"file21"]
      [0=>"file22"]
      [0=>"file23"]
      [0=>"file24"]
      [0=>"file25"]
      [0=>"file26"]
      [0=>"file27"]
      [0=>"file28"]
      [0=>"file29"]
      [0=>"file3"]
      [0=>"file30"]
      [0=>"file31"]
      [0=>"file32"]
      [0=>"file33"]
      [0=>"file34"]
      [0=>"file35"]
      [0=>"file36"]
      [0=>"file37"]
      [0=>"file38"]
      [0=>"file39"]
      [0=>"file4"]
      [0=>"file40"]
      [0=>"file41"]
      [0=>"file42"]
      [0=>"file43"]
      [0=>"file44"]
      [0=>"file45"]
      [0=>"file46"]
      [0=>"file47"]
      [0=>"file48"]
      [0=>"file49"]
      [0=>"file5"]
      [0=>"file50"]
      [0=>"file51"]
      [0=>"file52"]
      [0=>"file53"]
      [0=>"file54"]
      [0=>"file55"]
      [0=>"file56"]
      [0=>"file57"]
      [0=>"file58"]
      [0=>"file59"]
      [0=>"file6"]
      [0=>"file60"]
      [0=>"file61"]
      [0=>"file62"]
      [0=>"file63"]
      [0=>"file64"]
      [0=>"file65"]
      [0=>"file66"]
      [0=>"file67"]
      [0=>"file68"]
      [0=>"file69"]
      [0=>"file7"]
      [0=>"file70"]
      [0=>"file71"]
      [0=>"file72"]
      [0=>"file73"]
      [0=>"file74"]
      [0=>"file75"]
      [0=>"file76"]
      [0=>"file77"]
      [0=>"file78"]
      [0=>"file79"]
      [0=>"file8"]
      [0=>"file80"]
      [0=>"file81"]
      [0=>"file82"]
      [0=>"file83"]
      [0=>"file84"]
      [0=>"file85"]
      [0=>"file86"]
      [0=>"file87"]
      [0=>"file88"]
      [0=>"file89"]
      [0=>"file9"]
      [0=>"file90"]
      [0=>"file91"]
      [0=>"file92"]
      [0=>"file93"]
      [0=>"file94"]
      [0=>"file95"]
      [0=>"file96"]
      [0=>"file97"]
      [0=>"file98"]
      [0=>"file99"]
      Number of results 100 |}]
;;

let%expect_test _ =
    do_query [big_query()] file_query_not;
    [%expect {|
      query_rule(?0)
        +(Atom (`Var (1), `Int (7), `Var (0), `Ignore))
        -(Atom (`Var (1), `Int (8), `Val (`String ("bin")), `Ignore))
      [0=>"executable1"]
      [0=>"executable10"]
      [0=>"executable100"]
      [0=>"executable11"]
      [0=>"executable12"]
      [0=>"executable13"]
      [0=>"executable14"]
      [0=>"executable15"]
      [0=>"executable16"]
      [0=>"executable17"]
      [0=>"executable18"]
      [0=>"executable19"]
      [0=>"executable2"]
      [0=>"executable20"]
      [0=>"executable21"]
      [0=>"executable22"]
      [0=>"executable23"]
      [0=>"executable24"]
      [0=>"executable25"]
      [0=>"executable26"]
      [0=>"executable27"]
      [0=>"executable28"]
      [0=>"executable29"]
      [0=>"executable3"]
      [0=>"executable30"]
      [0=>"executable31"]
      [0=>"executable32"]
      [0=>"executable33"]
      [0=>"executable34"]
      [0=>"executable35"]
      [0=>"executable36"]
      [0=>"executable37"]
      [0=>"executable38"]
      [0=>"executable39"]
      [0=>"executable4"]
      [0=>"executable40"]
      [0=>"executable41"]
      [0=>"executable42"]
      [0=>"executable43"]
      [0=>"executable44"]
      [0=>"executable45"]
      [0=>"executable46"]
      [0=>"executable47"]
      [0=>"executable48"]
      [0=>"executable49"]
      [0=>"executable5"]
      [0=>"executable50"]
      [0=>"executable51"]
      [0=>"executable52"]
      [0=>"executable53"]
      [0=>"executable54"]
      [0=>"executable55"]
      [0=>"executable56"]
      [0=>"executable57"]
      [0=>"executable58"]
      [0=>"executable59"]
      [0=>"executable6"]
      [0=>"executable60"]
      [0=>"executable61"]
      [0=>"executable62"]
      [0=>"executable63"]
      [0=>"executable64"]
      [0=>"executable65"]
      [0=>"executable66"]
      [0=>"executable67"]
      [0=>"executable68"]
      [0=>"executable69"]
      [0=>"executable7"]
      [0=>"executable70"]
      [0=>"executable71"]
      [0=>"executable72"]
      [0=>"executable73"]
      [0=>"executable74"]
      [0=>"executable75"]
      [0=>"executable76"]
      [0=>"executable77"]
      [0=>"executable78"]
      [0=>"executable79"]
      [0=>"executable8"]
      [0=>"executable80"]
      [0=>"executable81"]
      [0=>"executable82"]
      [0=>"executable83"]
      [0=>"executable84"]
      [0=>"executable85"]
      [0=>"executable86"]
      [0=>"executable87"]
      [0=>"executable88"]
      [0=>"executable89"]
      [0=>"executable9"]
      [0=>"executable90"]
      [0=>"executable91"]
      [0=>"executable92"]
      [0=>"executable93"]
      [0=>"executable94"]
      [0=>"executable95"]
      [0=>"executable96"]
      [0=>"executable97"]
      [0=>"executable98"]
      [0=>"executable99"]
      Number of results 100 |}]
;;

let%expect_test _ =
    do_query [big_query()] file_query_indirect;
    [%expect {|
      bin_files(?0, ?1)
        +(Atom (`Var (2), `Int (8), `Var (1), `Ignore))
        +(Atom (`Var (2), `Int (7), `Var (0), `Ignore))
      [0=>"file1"]
      [0=>"file10"]
      [0=>"file100"]
      [0=>"file11"]
      [0=>"file12"]
      [0=>"file13"]
      [0=>"file14"]
      [0=>"file15"]
      [0=>"file16"]
      [0=>"file17"]
      [0=>"file18"]
      [0=>"file19"]
      [0=>"file2"]
      [0=>"file20"]
      [0=>"file21"]
      [0=>"file22"]
      [0=>"file23"]
      [0=>"file24"]
      [0=>"file25"]
      [0=>"file26"]
      [0=>"file27"]
      [0=>"file28"]
      [0=>"file29"]
      [0=>"file3"]
      [0=>"file30"]
      [0=>"file31"]
      [0=>"file32"]
      [0=>"file33"]
      [0=>"file34"]
      [0=>"file35"]
      [0=>"file36"]
      [0=>"file37"]
      [0=>"file38"]
      [0=>"file39"]
      [0=>"file4"]
      [0=>"file40"]
      [0=>"file41"]
      [0=>"file42"]
      [0=>"file43"]
      [0=>"file44"]
      [0=>"file45"]
      [0=>"file46"]
      [0=>"file47"]
      [0=>"file48"]
      [0=>"file49"]
      [0=>"file5"]
      [0=>"file50"]
      [0=>"file51"]
      [0=>"file52"]
      [0=>"file53"]
      [0=>"file54"]
      [0=>"file55"]
      [0=>"file56"]
      [0=>"file57"]
      [0=>"file58"]
      [0=>"file59"]
      [0=>"file6"]
      [0=>"file60"]
      [0=>"file61"]
      [0=>"file62"]
      [0=>"file63"]
      [0=>"file64"]
      [0=>"file65"]
      [0=>"file66"]
      [0=>"file67"]
      [0=>"file68"]
      [0=>"file69"]
      [0=>"file7"]
      [0=>"file70"]
      [0=>"file71"]
      [0=>"file72"]
      [0=>"file73"]
      [0=>"file74"]
      [0=>"file75"]
      [0=>"file76"]
      [0=>"file77"]
      [0=>"file78"]
      [0=>"file79"]
      [0=>"file8"]
      [0=>"file80"]
      [0=>"file81"]
      [0=>"file82"]
      [0=>"file83"]
      [0=>"file84"]
      [0=>"file85"]
      [0=>"file86"]
      [0=>"file87"]
      [0=>"file88"]
      [0=>"file89"]
      [0=>"file9"]
      [0=>"file90"]
      [0=>"file91"]
      [0=>"file92"]
      [0=>"file93"]
      [0=>"file94"]
      [0=>"file95"]
      [0=>"file96"]
      [0=>"file97"]
      [0=>"file98"]
      [0=>"file99"]
      Number of results 100 |}]
;;

let%expect_test "query_rule name clash" =
    do_query [link_db] {|
        [:find [value]
         :where 
         (query_rule value)
         [_ :edge 5]
         :rules
         [(query_rule value)
          [_ :edge value]]
       ]
    |}; 
    [%expect {|
      query_rule27774(?0)
        +(RuleRef ("query_rule", [`Var (0)]))
        +(Atom (`Ignore, `Int (7), `Val (`Int (5)), `Ignore))
      query_rule(?0)
        +(Atom (`Ignore, `Int (7), `Var (0), `Ignore))

      Number of results 0 |}]
;;
