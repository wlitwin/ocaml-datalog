open Ocaml_datalog_edn
module Db = Edn_db

let simple_cond_db =
    let open Db.Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let ed = attribute ed "file/name" Attr.One Db.Value.Types.db_string in
    let ed = datom ed id1 "file/name" (Val (`String "cool")) in
    let ed = datom ed id2 "file/name" (Val (`String "awesome")) in
    Db.add Db.empty ed
;;

let run_query db q p =
    let res = Db.query db q p in
    Caml.print_endline (Db.string_of_result res);
    (*Caml.print_endline (Db.to_string db);*)
    Printf.printf "Number of results %d\n" List.(length res);
;;

let do_query ?(funcs=Db.empty_funcs) db query =
    match Db.query_from_string ~funcs db query with
    | Error e -> failwith e
    | Ok (q, r, _) -> run_query db q r
;;

let%expect_test "validate EDN var order" =
    begin match Db.query_from_string [Db.empty] "[:find [a b c] :where [_ _ _]]" with
    | Ok (_, _, ["a"; "b"; "c"]) -> Caml.print_endline "Good"
    | Ok (_, _, lst) -> List.iter lst ~f:Caml.print_endline
    | Error e -> Caml.print_endline e
    end;
    [%expect {| Good |}]
;;

let%expect_test "validate entity id in position 1" =
    do_query [simple_cond_db] {|
        [:find [name] :where [#ref 0 :db/attribute name]]
    |};
  [%expect {|
    query_rule(?0)
      +(Atom (`Int (0), `Int (0), `Var (0), `Ignore))
    [0=>"db/attribute"]
    Number of results 1 |}]
;;

let%expect_test "should be the same as above test" =
    do_query [simple_cond_db] {|
        [:find [name] 
         :where 
         (= id #ref 0)
         [id :db/attribute name]
        ]
    |};
  [%expect {|
    query_rule(?0)
      +(Atom (`Var (1), `Int (0), `Var (0), `Ignore))
      +(Cond (Eq, `Var (1), `Int (0)))
    [0=>"db/attribute"]
    Number of results 1 |}]
;;

let%expect_test "get all attributes for entity" =
    do_query [simple_cond_db] {|
        [:find [name] 
         :where 
         [_ attr "awesome"]
         [attr :db/attribute name]
        ]
    |};
  [%expect {|
    query_rule(?0)
      +(Atom (`Var (1), `Int (0), `Var (0), `Ignore))
      +(Atom (`Ignore, `Var (1), `Val (`String ("awesome")), `Ignore))
    [0=>"file/name"]
    Number of results 1 |}]
;;

let%expect_test "validate doc attribute" =
    do_query [Db.empty] {|
        [:find [doc] :where [_ :db/doc doc]]
    |};
  [%expect {|
    query_rule(?0)
      +(Atom (`Ignore, `Int (3), `Var (0), `Ignore))
    [0=>"A transaction timestamp."]
    [0=>"Attribute name."]
    [0=>"Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity."]
    [0=>"Documentation about the attribute."]
    [0=>"The type of value the attribute will hold."]
    Number of results 5 |}]
;;

let%expect_test "get all attributes" =
    do_query [Db.empty] {|
        [:find [name type card doc]
         :where
         [id :db/attribute name]
         [id :db/valueType type]
         [id :db/cardinality card]
         [id :db/doc doc]
        ]
    |};
  [%expect {|
    query_rule(?0, ?1, ?2, ?3)
      +(Atom (`Var (4), `Int (0), `Var (0), `Ignore))
      +(Atom (`Var (4), `Int (2), `Var (1), `Ignore))
      +(Atom (`Var (4), `Int (1), `Var (2), `Ignore))
      +(Atom (`Var (4), `Int (3), `Var (3), `Ignore))
    [0=>"db/attribute"; 1=>"db/symbol"; 2=>"one"; 3=>"Attribute name."]
    [0=>"db/cardinality"; 1=>"db/cardinality"; 2=>"one"; 3=>"Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity."]
    [0=>"db/doc"; 1=>"db/string"; 2=>"one"; 3=>"Documentation about the attribute."]
    [0=>"db/transaction"; 1=>"db/timestamp"; 2=>"one"; 3=>"A transaction timestamp."]
    [0=>"db/valueType"; 1=>"db/type"; 2=>"one"; 3=>"The type of value the attribute will hold."]
    Number of results 5 |}]
;;
