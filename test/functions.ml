open Ocaml_datalog_edn
module Db = Edn_db

let simple_cond_db =
    let open Db.Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let ed = attribute ed "file/name" Attr.One Db.Value.Types.db_string in
    let ed = datom ed id1 "file/name" (Val (`String "cool")) in
    let ed = datom ed id2 "file/name" (Val (`String "awesome")) in
    Db.add Db.empty ed
;;

let simple_db1 =
    let open Db.Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let ed = attribute ed "file/name" Attr.One Db.Value.Types.db_string in
    let ed = attribute ed "file/hash" Attr.One Db.Value.Types.db_int in
    let ed = datom ed id1 "file/name" (Val (`String "cool")) in
    let ed = datom ed id1 "file/hash" (Val (`Int 10)) in
    let ed = datom ed id2 "file/name" (Val (`String "awesome")) in
    let ed = datom ed id2 "file/hash" (Val (`Int 20)) in
    Db.add Db.empty ed
;;

let simple_db_2 =
    let open Db.Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let id3, ed = make_id ed in
    let id4, ed = make_id ed in
    let ed = attribute ed "file/name" Attr.One Db.Value.Types.db_string in
    let ed = datom ed id1 "file/name" (Val (`String "cool")) in
    let ed = datom ed id2 "file/name" (Val (`String "awesome")) in
    let ed = datom ed id3 "file/name" (Val (`String "california")) in
    let ed = datom ed id4 "file/name" (Val (`String "bob")) in
    Db.add Db.empty ed
;;

let run_query db q p =
    let res = Db.query db q p in
    Caml.print_endline (Db.string_of_result res);
    (*Caml.print_endline (Db.to_string db);*)
    Printf.printf "Number of results %d\n" List.(length res);
;;

let do_query ?(funcs=Db.empty_funcs) db query =
    match Db.query_from_string ~funcs db query with
    | Error e -> failwith e
    | Ok (q, r, _) -> run_query db q r
;;

let starts_with_c : Db.Func.t =
    Predicate (function
        | [`Val (`String s)] -> String.is_prefix s ~prefix:"c"
        | _ -> false
    )
;;

let print_args =
    List.iter ~f:(function
        | `Val s -> Edn.to_string s |> Caml.print_endline
        | `Int i -> Caml.print_endline Int.(to_string i)
    )
;;

let bob_to_cool : Db.Func.t =
    General (function
        | [`Val (`String "bob")] -> [`Val (`String "cool")]
        | _ -> [`Val (`String "")]
    )
;;

let cool_to_20 : Db.Func.t =
    General (fun args ->
        match args with
        | [`Val (`String "cool")] -> [`Val (`Int 20)]
        | _ -> [`Val (`Int 0)]
    )
;;

let%expect_test "atom dependent on function" =
    let funcs = String.Map.of_alist_exn ["cool_to_20", cool_to_20] in
    let q = {|
        [:find [id name out id2 name2]
         :where
         [id :file/name name]
         [(#fun cool_to_20 name) out]
         [id2 :file/hash out]
         [id2 :file/name name2]
        ]
    |} in
    do_query ~funcs [simple_db1] q;
  [%expect {|
    function cool_to_20
    Outputs size: 1
    Output out -> 2
    query_rule(?0, ?1, ?2, ?3, ?4)
      +(Atom (`Var (0), `Int (8), `Var (1), `Ignore))
      +(Func (0, [`Var (1)], [2]))
      +(Atom (`Var (3), `Int (7), `Var (2), `Ignore))
      +(Atom (`Var (3), `Int (8), `Var (4), `Ignore))
    [0=>9; 1=>"cool"; 2=>20; 3=>10; 4=>"awesome"]
    Number of results 1 |}]
;;

let%expect_test "condition dependent on function output" =
    let funcs = String.Map.of_alist_exn ["bob_to_cool", bob_to_cool] in
    let q = {|
        [:find [id name out]
         :where
         [id :file/name name]
         (= out "cool")
         [(#fun bob_to_cool name) out]
        ]
    |} in
    do_query ~funcs [simple_db_2] q;
  [%expect {|
    function bob_to_cool
    Outputs size: 1
    Output out -> 2
    query_rule(?0, ?1, ?2)
      +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
      +(Func (0, [`Var (1)], [2]))
      +(Cond (Eq, `Var (2), `Val (`String ("cool"))))
    [0=>11; 1=>"bob"; 2=>"cool"]
    Number of results 1 |}]
;;

let%expect_test "user function predicate" =
    let funcs = String.Map.of_alist_exn ["starts_with_c", starts_with_c] in
    let q = {|
        [:find [id name] 
         :where
         [id :file/name name]
         [(#fun starts_with_c name)]
        ]
    |} in
    Caml.print_endline (Edn_sig.(sexp_of_t Edn.(from_string q)) |> Sexp.to_string_hum);
    do_query ~funcs [simple_cond_db] q;
  [%expect {|
    (Vector
     ((Keyword (() find)) (Vector ((Symbol (() id)) (Symbol (() name))))
      (Keyword (() where))
      (Vector ((Symbol (() id)) (Keyword ((file) name)) (Symbol (() name))))
      (Vector
       ((List ((Tag (() fun (Symbol (() starts_with_c)))) (Symbol (() name))))))))
    function starts_with_c
    Outputs size: 0
    query_rule(?0, ?1)
      +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
      +(Func (0, [`Var (1)], []))
    [0=>8; 1=>"cool"]
    Number of results 1 |}]
;;

let%expect_test "user function output" =
    let dup_fn : Db.Func.t = 
        General (fun args ->
            print_args args;
            match args with
            | [`Val (`String s)] -> [`Val (`String (s ^ s))]
            | _ -> [`Val (`String "")]
        )
    in
    let funcs = String.Map.of_alist_exn ["dup", dup_fn] in
    let q = {|
        [:find [id name dup_name] 
         :where
         [id :file/name name]
         [(#fun dup name) dup_name]
        ]
    |} in
    Caml.print_endline (Edn_sig.(sexp_of_t Edn.(from_string q)) |> Sexp.to_string_hum);
    do_query ~funcs [simple_cond_db] q;
  [%expect {|
    (Vector
     ((Keyword (() find))
      (Vector ((Symbol (() id)) (Symbol (() name)) (Symbol (() dup_name))))
      (Keyword (() where))
      (Vector ((Symbol (() id)) (Keyword ((file) name)) (Symbol (() name))))
      (Vector
       ((List ((Tag (() fun (Symbol (() dup)))) (Symbol (() name))))
        (Symbol (() dup_name))))))
    function dup
    Outputs size: 1
    Output dup_name -> 2
    query_rule(?0, ?1, ?2)
      +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
      +(Func (0, [`Var (1)], [2]))
    "awesome"
    "cool"
    "awesome"
    "cool"
    [0=>8; 1=>"cool"; 2=>"coolcool"]
    [0=>9; 1=>"awesome"; 2=>"awesomeawesome"]
    Number of results 2 |}]
;;

