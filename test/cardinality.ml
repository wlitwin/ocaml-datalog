open Ocaml_datalog_edn
module Db = Edn_db

let simple_card_db =
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let ed = datom ed id1 "value" (Val (`Int 200)) in
    Db.add Db.empty ed
;;

let simple_two_txn_db =
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let db = Db.add Db.empty ed in
    let ed = empty_deterministic in
    let ed = datom_full ed (eid_of_int 8) "value" (Val (`Int 200)) 3 in
    Db.add db ed
;;

let simple_three_txn_db =
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let db = Db.add Db.empty ed in
    let ed = empty_deterministic in
    let ed = datom_full ed (eid_of_int 8) "value" (Val (`Int 200)) 3 in
    let db = Db.add db ed in
    let ed = empty_deterministic in
    let ed = datom_full ed (eid_of_int 8) "value" (Val (`Int 400)) 4 in
    Db.add db ed
;;

let simple_many_db = 
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.Many Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let ed = datom ed id1 "value" (Val (`Int 200)) in
    Db.add Db.empty ed
;;

let run_query db q p =
    let res = Db.query db q p in
    Caml.print_endline (Db.string_of_result res);
    (*Caml.print_endline (Db.to_string db);*)
    Printf.printf "Number of results %d\n" List.(length res);
;;

let do_query db query =
    match Db.query_from_string db query with
    | Error e -> failwith e
    | Ok (q, r, _) -> run_query db q r
;;

let%expect_test "cardinality of 'one' should have 1 result" =
    do_query [simple_card_db] {|
    [:find [id value] :where [id :value value]]
    |};
  [%expect {|
    query_rule(?0, ?1)
      +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
    [0=>8; 1=>200]
    Number of results 1 |}]
;;

let%expect_test "cardinality of 'one' should have 1 result after two separate txns" =
    do_query [simple_two_txn_db] {|
    [:find [id value] :where [id :value value]]
    |};
  [%expect {|
    query_rule(?0, ?1)
      +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
    [0=>8; 1=>200]
    Number of results 1 |}]
;;

let%expect_test "cardinality of 'one' should have 1 result after three separate txns" =
    do_query [simple_three_txn_db] {|
    [:find [id value] :where [id :value value]]
    |};
  [%expect {|
    query_rule(?0, ?1)
      +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
    [0=>8; 1=>400]
    Number of results 1 |}]
;;

let%expect_test "cardinality of 'many' should have 2 results" =
    do_query [simple_many_db] {|
    [:find [id value] :where [id :value value]]
    |};
  [%expect {|
    query_rule(?0, ?1)
      +(Atom (`Var (0), `Int (7), `Var (1), `Ignore))
    [0=>8; 1=>100]
    [0=>8; 1=>200]
    Number of results 2 |}]
;;
