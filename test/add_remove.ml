open Ocaml_datalog_edn
module Db = Edn_db

let simple_db =
    let open Db.Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let ed = datom ed id1 "value" (Val (`Int 200)) in
    let db = Db.add Db.empty ed in
    (* Now delete an entry *)
    let ed = empty in
    let ed = datom_full ed (eid_of_int 8) "value" (Val (`Int 100)) 2 in
    Db.delete db ed
;;

let run_query db q p =
    let res = Db.query db q p in
    Caml.print_endline (Db.string_of_result res);
    (*Caml.print_endline (Db.to_string db);*)
    Printf.printf "Number of results %d\n" List.(length res);
;;

let do_query db query =
    match Db.query_from_string db query with
    | Error e -> failwith e
    | Ok (q, r, _) -> run_query db q r
;;

let%expect_test "delete an entry in the db" =
    do_query [simple_db] {|
        [:find [id value txn] :where [id :value value txn]]
    |};
  [%expect {|
    query_rule(?0, ?1, ?2)
      +(Atom (`Var (0), `Int (7), `Var (1), `Var (2)))
    [0=>8; 1=>200; 2=>2]
    Number of results 1 |}]
;;
