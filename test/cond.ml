open Ocaml_datalog_edn
module Db = Edn_db
open Db

let simple_cond_db =
    let open Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let ed = attribute ed "file/name" Attr.One Value.Types.db_string in
    let ed = datom ed id1 "file/name" (Val (`String "cool")) in
    let ed = datom ed id2 "file/name" (Val (`String "awesome")) in
    Db.add Db.empty ed
;;

let cond_db_2 =
    let open Transaction in
    let ed = empty in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let id3, ed = make_id ed in
    let id4, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 10)) in
    let ed = datom ed id2 "value" (Val (`Int 20)) in
    let ed = datom ed id3 "value" (Val (`Int 30)) in
    let ed = datom ed id4 "value" (Val (`Int 40)) in
    Db.add Db.empty ed
;;

let run_query db q p =
    let res = Db.query db q p in
    Caml.print_endline (Db.string_of_result res);
    (*Caml.print_endline (Db.to_string db);*)
    Printf.printf "Number of results %d\n" List.(length res);
;;

let do_query db query =
    match Db.query_from_string db query with
    | Error e -> failwith e
    | Ok (q, r, _) -> run_query db q r
;;

let%expect_test "cond top level" =
    do_query [simple_cond_db] {|
        [:find [name] 
         :where 
         [x :file/name name] 
         (= name "cool")
        ]
    |};
    [%expect {|
      query_rule(?0)
        +(Atom (`Var (1), `Int (7), `Var (0), `Ignore))
        +(Cond (Eq, `Var (0), `Val (`String ("cool"))))
      [0=>"cool"]
      Number of results 1 |}]
;;

let%expect_test "cond top level inverse" =
    do_query [simple_cond_db] {|
        [:find [name] 
         :where 
         (= name "cool")
         [x :file/name name] 
        ]
    |};
    [%expect {|
      query_rule(?0)
        +(Atom (`Var (1), `Int (7), `Var (0), `Ignore))
        +(Cond (Eq, `Var (0), `Val (`String ("cool"))))
      [0=>"cool"]
      Number of results 1 |}]
;;

let%expect_test "cond top level not" =
    do_query [simple_cond_db] {|
        [:find [name] 
         :where 
         (not (= name "cool"))
         [x :file/name name] 
        ]
    |};
    [%expect {|
      query_rule(?0)
        +(Atom (`Var (1), `Int (7), `Var (0), `Ignore))
        -(Cond (Eq, `Var (0), `Val (`String ("cool"))))
      [0=>"awesome"]
      Number of results 1 |}]
;;

let%expect_test "cond top level not inverse" =
    do_query [simple_cond_db] {|
        [:find [name] 
         :where 
         [x :file/name name] 
         (not (= name "cool"))
        ]
    |};
    [%expect {|
      query_rule(?0)
        +(Atom (`Var (1), `Int (7), `Var (0), `Ignore))
        -(Cond (Eq, `Var (0), `Val (`String ("cool"))))
      [0=>"awesome"]
      Number of results 1 |}]
;;

let%expect_test "cond multiple top level" =
    do_query [cond_db_2] {|
        [:find [value] 
         :where 
         (> value 10)
         (< value 40)
         [_ :value value]
        ]
    |};
    [%expect {|
      query_rule(?0)
        +(Atom (`Ignore, `Int (7), `Var (0), `Ignore))
        +(Cond (Lt, `Var (0), `Val (`Int (40))))
        +(Cond (Gt, `Var (0), `Val (`Int (10))))
      [0=>20]
      [0=>30]
      Number of results 2 |}]
;;

let%expect_test "cond multiple top level or rule" =
    do_query [cond_db_2] {|
        [:find [value] 
         :where (exclusive value)
         :rules
         [(exclusive value)
          [_ :value value]
          (<= value 10)
         ]
         [(exclusive value)
          [_ :value value]
          (>= value 40)
         ]
        ]
    |};
    [%expect {|
      exclusive(?0)
        +(Atom (`Ignore, `Int (7), `Var (0), `Ignore))
        +(Cond (Gte, `Var (0), `Val (`Int (40))))
      exclusive(?0)
        +(Atom (`Ignore, `Int (7), `Var (0), `Ignore))
        +(Cond (Lte, `Var (0), `Val (`Int (10))))
      [0=>10]
      [0=>40]
      Number of results 2 |}]
;;
