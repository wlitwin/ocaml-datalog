open Ocaml_datalog_edn
module Db = Edn_db

let simple_card_db =
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let ed = datom ed id1 "value" (Val (`Int 200)) in
    Db.add Db.empty ed
;;

let run_query db q p =
    let res = Db.query db q p in
    Caml.print_endline (Db.string_of_result res);
    (*Caml.print_endline (Db.to_string db);*)
    Printf.printf "Number of results %d\n" List.(length res);
;;

let do_query db query =
    match Db.query_from_string db query with
    | Error e -> failwith e
    | Ok (q, r, _) -> run_query db q r
;;

let%expect_test "make sure the transaction attributes exist" =
    do_query [Db.empty] {|
    [:find [id value] :where [id :db/transaction value]]
    |};
  [%expect {|
    query_rule(?0, ?1)
      +(Atom (`Var (0), `Int (4), `Var (1), `Ignore))
    [0=>5; 1=>0]
    [0=>6; 1=>0]
    Number of results 2 |}]
;;

let%expect_test "make sure all the attributes exist" =
    do_query [Db.empty] {|
    [:find [id name type card doc] 
     :where 
     [id :db/attribute name]
     [id :db/valueType type]
     [id :db/cardinality card]
     [id :db/doc doc]
    ]
    |};
    [%expect {|
      query_rule(?0, ?1, ?2, ?3, ?4)
        +(Atom (`Var (0), `Int (0), `Var (1), `Ignore))
        +(Atom (`Var (0), `Int (2), `Var (2), `Ignore))
        +(Atom (`Var (0), `Int (1), `Var (3), `Ignore))
        +(Atom (`Var (0), `Int (3), `Var (4), `Ignore))
      [0=>0; 1=>"db/attribute"; 2=>"db/symbol"; 3=>"one"; 4=>"Attribute name."]
      [0=>1; 1=>"db/cardinality"; 2=>"db/cardinality"; 3=>"one"; 4=>"Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity."]
      [0=>2; 1=>"db/valueType"; 2=>"db/type"; 3=>"one"; 4=>"The type of value the attribute will hold."]
      [0=>3; 1=>"db/doc"; 2=>"db/string"; 3=>"one"; 4=>"Documentation about the attribute."]
      [0=>4; 1=>"db/transaction"; 2=>"db/timestamp"; 3=>"one"; 4=>"A transaction timestamp."]
      Number of results 5 |}]
;;

let%expect_test "make sure all the attributes exist after one transaction" =
    do_query [simple_card_db] {|
    [:find [id name type card doc] 
     :where 
     [id :db/attribute name]
     [id :db/valueType type]
     [id :db/cardinality card]
     [id :db/doc doc]
    ]
    |};
  [%expect {|
    query_rule(?0, ?1, ?2, ?3, ?4)
      +(Atom (`Var (0), `Int (0), `Var (1), `Ignore))
      +(Atom (`Var (0), `Int (2), `Var (2), `Ignore))
      +(Atom (`Var (0), `Int (1), `Var (3), `Ignore))
      +(Atom (`Var (0), `Int (3), `Var (4), `Ignore))
    [0=>0; 1=>"db/attribute"; 2=>"db/symbol"; 3=>"one"; 4=>"Attribute name."]
    [0=>1; 1=>"db/cardinality"; 2=>"db/cardinality"; 3=>"one"; 4=>"Cardinality of attribute. One means a single value for this attribute per entity. Many means multiple entries for the attribute per entity."]
    [0=>2; 1=>"db/valueType"; 2=>"db/type"; 3=>"one"; 4=>"The type of value the attribute will hold."]
    [0=>3; 1=>"db/doc"; 2=>"db/string"; 3=>"one"; 4=>"Documentation about the attribute."]
    [0=>4; 1=>"db/transaction"; 2=>"db/timestamp"; 3=>"one"; 4=>"A transaction timestamp."]
    Number of results 5 |}]
;;
