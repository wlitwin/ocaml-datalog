open Ocaml_datalog_edn
module Db = Edn_db

let db_single =
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let id2, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let ed = datom ed id2 "value" (Val (`Int 200)) in
    Db.add Db.empty ed
;;

let db_many =
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.Many Db.Value.Types.db_int in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let ed = datom ed id1 "value" (Val (`Int 200)) in
    Db.add Db.empty ed
;;

let db_mixed =
    let open Db.Transaction in
    let ed = empty_deterministic in
    let id1, ed = make_id ed in
    let ed = attribute ed "value" Attr.One Db.Value.Types.db_int in
    let ed = attribute ed "value2" Attr.Many Db.Value.Types.db_string in
    let ed = datom ed id1 "value" (Val (`Int 100)) in
    let ed = datom ed id1 "value2" (Val (`String "first value")) in
    let ed = datom ed id1 "value2" (Val (`String "second value")) in
    Db.add Db.empty ed
;;

let string_of_entity (e : Db.entity) =
    let prop_to_str = function
        | Db.Single v -> "Single: " ^ Db.Value.to_string v
        | Multi lst ->
            Printf.sprintf "Many: [%s]"
                (String.concat ~sep:"; " List.(map ~f:Db.Value.to_string lst))
    in
    let props = 
        e.attributes
        |> String.Map.to_alist
        |> List.map ~f:(fun (name, prop) -> Printf.sprintf "    %s = %s" name (prop_to_str prop))
        |> String.concat ~sep:"\n"
    in
    Printf.sprintf "{ id = %d\n  props:\n%s\n}\n" e.id props
;;

let%expect_test "Cardinality one test" =
    let e = Db.get_entity db_single 8 in
    begin match e with
    | None -> Caml.print_endline "GOT NONE - THIS IS WRONG"
    | Some e -> Caml.print_endline (string_of_entity e);
    end;
    [%expect {|
      { id = 8
        props:
          value = Single: 100
      } |}]
;;

let%expect_test "Cardinality many test" =
    let e = Db.get_entity db_many 8 in
    begin match e with
    | None -> Caml.print_endline "GOT NONE - THIS IS WRONG"
    | Some e -> Caml.print_endline (string_of_entity e);
    end;
    [%expect {|
      { id = 8
        props:
          value = Many: [100; 200]
      } |}]
;;

let%expect_test "Mixed cardinality test" =
    let e = Db.get_entity db_mixed 9 in
    begin match e with
    | None -> Caml.print_endline "GOT NONE - THIS IS WRONG"
    | Some e -> Caml.print_endline (string_of_entity e);
    end;
    [%expect {|
      { id = 9
        props:
          value = Single: 100
          value2 = Many: ["first value"; "second value"]
      } |}]
;;
