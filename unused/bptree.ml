module type KeySig = sig
  type t
  [@@deriving sexp, compare, equal, bin_io]
end

module Make(Key : KeySig) : sig
    type 'a t
    [@@deriving sexp, equal, compare, bin_io]

    val create : child_count:int -> 'a t
    val iter : 'a t -> f:('a -> unit) -> unit
    val iter2 : 'a t -> f:('a -> unit) -> unit
    val fold : 'a t -> init:'b -> f:(key:Key.t -> data:'a -> 'b -> 'b) -> 'b
    val insert : 'a t -> Key.t -> 'a -> 'a t
    val update : 'a t -> Key.t -> f:('a option -> 'a) -> 'a t
    val remove : 'a t -> Key.t -> 'a t
    val for_all : 'a t -> f:('a -> bool) -> bool
    val find : 'a t -> Key.t -> 'a option
    val length : 'a t -> int
    val to_string : 'a t -> ('a -> Sexp.t) -> string
    val to_string_hum : 'a t -> ('a -> string) -> string
    val get_range_start : 'a t -> Key.t -> 'a Sequence.t

    (* TODO range query *)
    (* TODO - map, update *)

end = struct
    module MKey = Key
    module KeyMap = struct
        module Map = Map.Make(MKey)
        include Map
        include Map.Provide_bin_io(MKey)
    end
    (*module KeyMap = ImmMap.Make(Key)*)

    type 'a leaf_node = 'a KeyMap.t
    [@@deriving sexp, equal, compare, bin_io]

    type 'a internal_node = 'a node KeyMap.t
    [@@deriving sexp, equal, compare, bin_io]

    and 'a node = Internal of 'a internal_node
             | Leaf of 'a leaf_node
    [@@deriving sexp, equal, compare, bin_io]

    type 'a t = {
        num_child_nodes : int;
        root : 'a node;
        (* Store faster list of leafs *)
        leaves : 'a leaf_node KeyMap.t;
    }[@@deriving sexp, equal, compare, bin_io]

    let create ~child_count = 
        assert (child_count > 2);
        {
            num_child_nodes = child_count;
            root = Leaf KeyMap.empty;
            leaves = KeyMap.empty;
        }

    let value_exn o = Option.value_exn o
    let max_elt_exn leaf =
        KeyMap.max_elt_exn leaf |> fst
    ;;

    let find_index map key =
        (* TODO - this can probably remove the begin min_elt case, 
         * as it should be greater than the key in that case *)
        (*let min_elt, _ = KeyMap.min_elt_exn map in
        if Key.compare key min_elt <= 0 then (
           Some (min_elt, KeyMap.find_exn map min_elt)
        ) else ( *)
            let max_elt, _ = KeyMap.max_elt_exn map in
            if Key.compare key max_elt > 0 then (
                Some (max_elt, KeyMap.find_exn map max_elt)
            ) else (
                (* Bin search ? *)
                KeyMap.binary_search map ~compare:(fun ~key ~data:_ other ->
                    Key.compare key other
                ) `First_greater_than_or_equal_to key
            )
        (* ) *)
    ;;

    let get_leaf (t : 'a t) key =
        let rec loop path = function
            | Internal internal ->
                begin match find_index internal key with
                | Some (key, node) -> loop ((key, internal) :: path) node
                | None -> None
                end
            | Leaf node ->
                match find_index node key with
                | None -> None
                | Some (key, value) -> Some (path, key, node, value)
        in
        loop [] t.root
    ;;

    let find (t : 'a t) key =
        match t.root with
        | Leaf node -> KeyMap.find node key
        | Internal _ ->
            match get_leaf t key with
            | None -> None
            | Some (_, found_key, _, value) -> 
                if Key.compare found_key key = 0 then (
                    Some value
                ) else None
    ;;

    (*
     * val to_sequence :
     *     ?order:[ `Decreasing_key | `Increasing_key ] ->
     *     ?keys_greater_or_equal_to:Key.t ->
     *     ?keys_less_or_equal_to:Key.t -> 'a t -> (Key.t * 'a) Base__.Sequence.t
     *)

    let seq_of_leaf (node : 'a leaf_node) (key : Key.t) =
        KeyMap.to_sequence ~order:`Increasing_key ~keys_greater_or_equal_to:key node
        |> Sequence.map ~f:snd
    ;;

    let sequence_of_rest_leaves (leaves : 'a leaf_node KeyMap.t)  (next : Key.t) : 'a Sequence.t =
            (KeyMap.to_sequence ~order:`Increasing_key ~keys_greater_or_equal_to:next leaves)
                (* When we dereference the first leaf, we must chomp it to the right location *)
            |> Sequence.bind ~f:(fun (_, map) ->
                KeyMap.to_sequence map |> Sequence.map ~f:snd
            )
    ;;

    let get_range_start t (key : Key.t) =
        match t.root with
        | Leaf node -> seq_of_leaf node key
        | Internal _ ->
            match get_leaf t key with
            | None -> 
                    (*Caml.print_endline "KEY NOT FOUND";*)
                    Sequence.empty
            | Some (_, found_key, node, _) ->
                (*Caml.print_endline "KEY FOUND";
                Printf.printf "INPUT %s\nFOUND %s\n" 
                    (Key.sexp_of_t key |> Sexp.to_string_hum) 
                    (Key.sexp_of_t found_key |> Sexp.to_string_hum);*)
                begin match KeyMap.closest_key t.leaves `Greater_than found_key with
                | None -> seq_of_leaf node key
                | Some (next, _) -> 
                    Sequence.append
                        (seq_of_leaf node key)
                        (sequence_of_rest_leaves t.leaves next)
                end
    ;;

    let split_leaf map key data =
        let map = KeyMap.set map ~key ~data in
        (*Printf.printf "%s\n" (map |> KeyMap.sexp_of_t Value.sexp_of_t |> Sexp.to_string_hum);*)
        let len = KeyMap.length map in
        let mid, _ = KeyMap.nth_exn map ((len+1)/2) in
        (*Printf.printf "LEN %d (LEN+1)/2 %d MID %s\n" len ((len+1)/2) (Key.sexp_of_t mid |> Sexp.to_string_hum);*)
        let left, mid, right = KeyMap.split map mid in
        match mid with
        | None -> 
            let leaf1 = left in
            let leaf2 = right in
            leaf1, leaf2
        | Some (mid, data) ->
            let leaf1 = left in
            let leaf2 = KeyMap.set right ~key:mid ~data in
            assert (KeyMap.length leaf1 + KeyMap.length leaf2 = len);
            leaf1, leaf2
    ;; 

    type 'a insert_leaf_result = Split of (Key.t * Key.t * 'a KeyMap.t * Key.t * 'a KeyMap.t)
                               | NoSplit of 'a leaf_node

    let insert_leaf max_children node key value =
        if KeyMap.length node >= max_children then (
            (* Split *)
            (* Need to update our iteration map... *)
            let old_max = max_elt_exn node in
            let leaf1, leaf2 = split_leaf node key value in
            let kleaf1 = max_elt_exn leaf1 in
            let kleaf2 = max_elt_exn leaf2 in
            (*Some (old_max, kleaf1, leaf1, kleaf2, leaf2), Internal map*)
            Split (old_max, kleaf1, leaf1, kleaf2, leaf2)
        ) else (
            (* Max must be the same because find says >= key *)
            (*None, Leaf (KeyMap.set node ~key ~data:value)*)
            NoSplit (KeyMap.set node ~key ~data:value)
        )
    ;;

    let max_elt_node_exn = function
        | Leaf node -> max_elt_exn node
        | Internal node -> max_elt_exn node

    let propagate_up path leaf =
        let rec loop acc = function
            | [] -> acc
            | (key, parent) :: tl ->
                (* This is wrong, we keep the reference to the one leaf 
                 * and add the split one *)
                (* Need to reset key in parent *)
                let parent = KeyMap.remove parent key in
                let parent = Internal (KeyMap.set parent ~key:(max_elt_node_exn acc) ~data:acc) in
                loop parent tl
        in
        loop leaf path
    ;;

    let to_string (t : 'a t) conv =
        t |> sexp_of_t conv |> Sexp.to_string_hum
    ;;

    let to_string_hum (t : 'a t) str_elem =
        let pad cnt = String.init cnt ~f:(fun _ -> ' ') in
        let str_key key = key |> Key.sexp_of_t |> Sexp.to_string_hum in
        let rec str ident = function
            | Leaf map -> 
                "[" ^
                KeyMap.fold map ~init:"" ~f:(fun ~key ~data acc ->
                    acc ^ Printf.sprintf "%s:%s " (str_key key) (str_elem data)
                ) ^ "]\n"
            | Internal map ->
                pad ident ^ "\n" ^
                KeyMap.fold map ~init:"" ~f:(fun ~key ~data acc ->
                    acc ^ pad (ident+2) ^ (str_key key) ^ ": " ^ str (ident+2) data
                )
        in
        let str_leaves leaves =
            KeyMap.fold leaves ~init:"" ~f:(fun ~key ~data acc ->
                acc ^ str_key key ^ ": [" ^
                KeyMap.fold data ~init:"" ~f:(fun ~key ~data acc ->
                    acc ^ Printf.sprintf "(%s %s)" (str_key key) (str_elem data)
                ) ^ "]\n"
            )
        in
        str 0 t.root ^
        str_leaves t.leaves
    ;;

    let iter (t : 'a t) ~f =
        KeyMap.iter t.leaves ~f:(fun leaf ->
            KeyMap.iter leaf ~f
        )
    ;;

    let iter2 (t : 'a t) ~f =
        let rec loop = function
            | Internal node ->
                KeyMap.iter node ~f:loop
            | Leaf node ->
                KeyMap.iter node ~f
        in
        loop t.root
    ;;

    let fold t ~init ~f =
        KeyMap.fold t.leaves ~init ~f:(fun ~key:_ ~data acc ->
            KeyMap.fold data ~init:acc ~f
        )
    ;;

    let for_all t  ~f =
        KeyMap.for_all t.leaves ~f:(fun key ->
            KeyMap.for_all key ~f
        )

    let length t =
        (* TODO - make constant time... *)
        KeyMap.fold t.leaves ~init:0 ~f:(fun ~key:_ ~data count ->
            count + KeyMap.length data
        )
    ;;

    let update_leave_keys leaves (old_key, kleaf1, leaf1, kleaf2, leaf2) = 
        leaves
        |> (fun map -> KeyMap.remove map old_key)
        |> KeyMap.set ~key:kleaf1 ~data:leaf1
        |> KeyMap.set ~key:kleaf2 ~data:leaf2
    ;;

    let insert_internal t key value =
        match get_leaf t key with
        | None -> failwith "Shouldn't be none"
        | Some (path, _, leaf, _) ->
            path, insert_leaf t.num_child_nodes leaf key value
    ;;

    let make_internal_from_split kleaf1 leaf1 kleaf2 leaf2 =
        let map = KeyMap.singleton kleaf1 leaf1 in
        let map = KeyMap.set map ~key:kleaf2 ~data:leaf2 in
        Internal map
    ;;

    let rec merge_split_up t (_, parent) path (old_max, kleaf1, leaf1, kleaf2, leaf2) =
        (* Fixup immediate parent *)
        let parent = KeyMap.remove parent old_max in
        let parent = KeyMap.set parent ~key:kleaf1 ~data:leaf1 in
        if KeyMap.length parent >= t.num_child_nodes then (
            (* Split ... *)
            let pleft, pright = split_leaf parent kleaf2 leaf2 in
            let pkey1 = max_elt_exn pleft in
            let pkey2 = max_elt_exn pright in
            match path with
            | [] -> 
                (* Build new internal node *)
                make_internal_from_split pkey1 (Internal pleft) pkey2 (Internal pright)
            | (old_key, _ as hd) :: tl -> 
                    merge_split_up t hd tl (old_key, pkey1, Internal pleft, pkey2, Internal pright)
        ) else (
            (* Insert leaf2 directly *)
            let parent = Internal (KeyMap.set parent ~key:kleaf2 ~data:leaf2) in
            propagate_up path parent
        )
    ;;

    let insert (t : 'a t) (key : Key.t) (value : 'a) =
        match t.root with
        | Leaf node -> 
                begin match insert_leaf t.num_child_nodes node key value with
                | NoSplit leaf ->
                    let leaves = KeyMap.singleton key leaf in
                    {t with root=Leaf leaf; leaves}
                | Split (_, kleaf1, leaf1, kleaf2, leaf2) -> 
                    let leaves = KeyMap.singleton kleaf1 leaf1 in
                    let leaves = KeyMap.set leaves ~key:kleaf2 ~data:leaf2 in
                    let root = make_internal_from_split kleaf1 (Leaf leaf1) kleaf2 (Leaf leaf2) in
                    {t with root; leaves}
                end
        | Internal _ -> 
                match insert_internal t key value with
                | (((key, _) :: _) as path), (NoSplit node) -> 
                    (* Fix the leaves *)
                    (*Caml.print_endline "Case 1";*)
                    let leaves = KeyMap.remove t.leaves key in
                    let leaves = KeyMap.set leaves ~key:(max_elt_exn node) ~data:node in
                    {t with root = propagate_up path (Leaf node); leaves}
                | [], (Split _) -> failwith "Expected 1 parent (split)"
                | _, (NoSplit _) -> failwith "Expected at least 1 parent (no split)"
                | parent :: rest , (Split (old_key, k1, l1, k2, l2 as data)) ->
                    (*Caml.print_endline "Case 2";*)
                    (* need to recurse up splitting parents as needed *)
                    let node = merge_split_up t parent rest (old_key, k1, Leaf l1, k2, Leaf l2) in
                    let leaves = update_leave_keys t.leaves data in
                    { t with root=node; leaves}
    ;;

    let update (t : 'a t) key ~f =
        (* TODO - make better implementation *)
        insert t key (f (find t key))
    ;;

    let delete_up path =
        let rec loop acc = function
            | [] -> acc
            | (key, parent) :: tl ->
                let children = KeyMap.remove parent key in
                if KeyMap.is_empty children then (
                    loop acc tl
                ) else (
                    Internal children
                )
        in
        loop (Leaf KeyMap.empty) path
    ;;

    let merge_maps map1 map2 =
        (*
        KeyMap.merge map1 map2 ~f:(fun ~key:_ kind ->
            match kind with
            | `Both (a, _) -> Some a
            | `Left a -> Some a
            | `Right a -> Some a
        )*)
        KeyMap.append ~lower_part:map1 ~upper_part:map2
        |> function
           | `Ok map -> map
           | `Overlapping_key_ranges -> failwith "Expected non-overlapping keys"
    ;;

    let swap_parent_key path new_key node =
        match path with
        | [] -> (*Leaf node*) failwith "Impossible" (* I think *)
        | (key, parent) :: tl ->
            let children = KeyMap.remove parent key in
            let children = KeyMap.set children ~key:new_key ~data:(Leaf node) in
            (* Propagate this up the rest of the way *)
            let parent = children in
            propagate_up tl (Internal parent)
    ;;

    let merge_neighbors t (node_key, node) (neighbor_key, neighbor) =
        (*
        Caml.print_endline "MERGING NEIGHBORS";
        Printf.printf "NODE %s\n" (node.records |> KeyMap.sexp_of_t Value.sexp_of_t |> Sexp.to_string_hum);
        Printf.printf "NEIGHBOR %s\n" (neighbor.records |> KeyMap.sexp_of_t Value.sexp_of_t |> Sexp.to_string_hum);
        *)
        (* This part is tricky because we have to modify two paths
         * in the tree without becoming inconsistent
         *)
        (* Merge the two leave node records *)
        let merged = merge_maps node neighbor in

        (* Delete neighbor, and then refind our path *)
        let neighbor_path, _, _, _ = get_leaf t neighbor_key |> value_exn in
        let t = { t with root = delete_up neighbor_path } in

        let path, _, _, _ = get_leaf t node_key |> value_exn in
        (* Manually do this, need to delete old key from parent and add new one... *)
        let new_key = max_elt_exn merged in
        let root = swap_parent_key path new_key merged in 

        (* Fix the leave references *)
        let leaves =
            t.leaves 
            |> (fun map -> KeyMap.remove map neighbor_key)
            |> (fun map -> KeyMap.remove map node_key)
            |> KeyMap.set ~key:new_key ~data:merged
        in
        { t with root; leaves }
    ;;

    let get_first_path_key = function
        | (key, _) :: _ -> key
        | _ -> failwith "Expected non-empty path"
    ;;

    let remove_check_for_merge t path old_key node =
        let num_records = KeyMap.length node in
        if num_records < t.num_child_nodes/2 then (
            (* See if we can rebalance/merge with neighbor *)
            match KeyMap.closest_key t.leaves `Greater_than old_key with
            | None -> (* No neighbor to merge with... ignore *)
                { t with root = propagate_up path (Leaf node) }
            | Some (neighbor_key, neighbor) ->
                assert (not (phys_equal neighbor node));
                let neighbor_records = KeyMap.length neighbor in
                if neighbor_records + num_records < t.num_child_nodes then (
                    (* Merge *)
                    merge_neighbors t (max_elt_exn node, node) (neighbor_key, neighbor)
                ) else (
                    (* Abort *)
                    { t with root = propagate_up path (Leaf node) }
                )
        ) else (
            (* We're good, do nothing *)
            (*Caml.print_endline "propagate up...";*)
            let leaf_key = get_first_path_key path in
            let leaves = KeyMap.set t.leaves ~key:leaf_key ~data:node in
            { t with root = propagate_up path (Leaf node); leaves }
        )
    ;;

    let remove (t : 'a t) (key : Key.t) =
        match t.root with
        | Leaf node -> 
            Caml.print_endline "Delete case 1";
            let node = KeyMap.remove node key in
            { t with root = Leaf node }
        | Internal _ ->
            match get_leaf t key with
            | None -> t
            | Some (path, found_key, found_node, _) -> 
                if Key.compare found_key key = 0 then (
                    let node = KeyMap.remove found_node key in
                    match KeyMap.is_empty node with
                    | false ->
                        (*Caml.print_endline "Delete case 3";*)
                        (* Check if this node still has the proper number of records *)
                        remove_check_for_merge t path found_key node
                    | true ->
                        (* Delete this upwards *)
                        (*Caml.print_endline "Delete case 4";*)
                        let leaf_key = get_first_path_key path in
                        let leaves = KeyMap.remove t.leaves leaf_key in
                        { t with root = delete_up path; leaves }
                ) else (
                    (* Key doesn't actually exist *)
                    t
                )
    ;;
end
