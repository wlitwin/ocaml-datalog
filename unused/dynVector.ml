type 'a t = {
    mutable len : int;
    mutable arr : 'a array;
}

let create ?(reserve=10) ?(len=0) value = {
    len;
    arr = Array.create ~len:reserve value;
}

let init len ~f = {
    len;    
    arr = Array.init len ~f
}

let copy t = {
    t with arr = Array.copy t.arr
}

let clear t =
    t.len <- 0

let is_empty t = t.len = 0
let length t = t.len

let calc_length_increase len =
    Caml.float (max len 10) *. 1.5 |> Int.of_float

let resize t =
    let arr = Array.create ~len:(calc_length_increase t.len) t.arr.(0) in
    Array.blito ~src:t.arr ~dst:arr ();
    t.arr <- arr 
;;

let append t value =
   if t.len >= Array.length t.arr then (
       resize t
   );
   t.arr.(t.len) <- value;
   t.len <- t.len + 1;
;;

let concat t t2 =
    let len = Array.length t.arr in
    let total_len = t.len + t2.len in
    if total_len <= len then (
        Array.blito 
            ~src:t2.arr ~src_len:t2.len
            ~dst:t.arr ~dst_pos:t.len
            ()
    ) else (
        (* Need to resize ourselves *)
        let new_size = calc_length_increase total_len in
        let arr = Array.create ~len:new_size t.arr.(0) in
        Array.blito ~src:t.arr ~dst:arr ();
        Array.blito 
            ~src:t2.arr ~src_len:t2.len
            ~dst:arr ~dst_pos:t.len
            ();
        t.len <- total_len;
        t.arr <- arr;
    )
;;

let filter t ~f =
    let ptr = ref 0 in
    for i=0 to t.len-1 do
        if not (f (t.arr.(i))) then (
            t.arr.(!ptr) <- t.arr.(i);
            incr ptr;
        )
    done;
    t.len <- !ptr
;;

let fold t ~init ~f =
    let acc = ref init in
    for i=0 to t.len-1 do
        acc := f !acc t.arr.(i)
    done;
    !acc
;;

let iter t ~f =
    for i=0 to t.len-1 do
        f t.arr.(i)
    done;
;;

let set t idx value =
    assert (idx >= 0 && idx < t.len);
    t.arr.(idx) <- value
;;

let%expect_test _ =
    let arr = create ~reserve:10 0 in
    for i=0 to 9 do
        append arr i
    done;
    filter arr ~f:(fun x -> x mod 2 = 0);
    iter arr ~f:(fun x -> Printf.printf "%d " x);
    [%expect {| 1 3 5 7 9 |}]

let%expect_test _ =
    let arr1 = init 10 ~f:(fun x -> x) in
    let arr2 = init 10 ~f:(fun x -> x + 10) in
    concat arr1 arr2;
    iter arr1 ~f:(fun x -> Printf.printf "%d " x);
    [%expect {| 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 |}]

