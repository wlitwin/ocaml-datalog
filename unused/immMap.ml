module type KeySig = sig
  type t
  [@@deriving sexp, compare, equal, bin_io]
end

module Make(Key : KeySig) = struct
    type 'a t = (Key.t * 'a) array
    [@@deriving sexp, compare, equal, bin_io]

    let find_internal t ?(kind=`First_equal_to) key =
        Array.binary_search t kind ~compare:(fun (a, _) b ->
            Key.compare a b
        ) key 

    let find t key =
        find_internal t key
        |> function
                | None -> None
                | Some idx -> Some (t.(idx) |> snd)
    ;;

    let add_size_1 t value : 'a t =
        let len = Array.length t in
        let a = Array.init (len+1) ~f:(fun idx ->
            if idx < len then (
                t.(idx) 
            ) else (
                value
            )
        );
        in
        Array.sort a ~compare:(fun (k1, _) (k2, _) ->
            Key.compare k1 k2
        );
        a

    let binary_search t ~compare kind (key : Key.t) =
        Array.binary_search t kind ~compare:(fun (a, data) (b : Key.t) ->
            compare ~key:a ~data:data b 
        ) key |> function
                 | None -> None
                 | Some idx -> Some t.(idx)
    ;;

	(*  val closest_key :
		'a t ->
		[ `Greater_or_equal_to | `Greater_than | `Less_or_equal_to | `Less_than ] ->
		Key.t -> (Key.t * 'a) option
	*)
    let closest_key t kind key =
		match kind with
		| `Greater_than -> 
            (*Printf.printf "CLOSEST KEY our len %d %s\n" (Array.length t) (key |> Key.sexp_of_t |> Sexp.to_string_hum);*)
			Array.binary_search t `First_strictly_greater_than ~compare:(fun (a, _) b ->
				Key.compare a b
			) key
			|> (function
				| None -> 
                        (*Caml.print_endline "Closest not found";*)
                        None
				| Some idx -> 
                        (*Printf.printf "CLOSEST found %s\n" (t.(idx) |> fst |> Key.sexp_of_t |> Sexp.to_string_hum);*)
                        Some t.(idx)
			)
		| _ -> failwith "Only greater than implemented"
	;;

	let nth_exn t i = t.(i)

    let to_sequence ?order:_ ?keys_greater_or_equal_to t =
        let key = match keys_greater_or_equal_to with
                | None -> t.(0) |> fst
                | Some key -> key
        in
        (*Printf.printf "To SEQUENCE %s\n" (key |> Key.sexp_of_t |> Sexp.to_string_hum);*)
        let idx = Array.binary_search t `First_greater_than_or_equal_to ~compare:(fun (a, _) b ->
            Key.compare a b
        ) key in
        match idx with
        | None -> 
                Caml.print_endline "Not found";
                Sequence.empty
        | Some start ->
            (*Printf.printf "Start %d len %d size %d\n" start (Array.length t) (Array.length t - start - 1);*)
            Sequence.init (Array.length t - start) ~f:(fun idx -> 
                (*Caml.print_endline "SEQ";*)
                t.(idx + start))
    ;;

    let length = Array.length

    let empty = [||]

	(* val split : 'a t -> Key.t -> 'a t * (Key.t * 'a) option * 'a t *)
	let split t key =
        (*Printf.printf "Split %s\n" (key |> Key.sexp_of_t |> Sexp.to_string_hum);*)
		match find_internal t ~kind:`First_greater_than_or_equal_to key with
		| None -> t, None, empty
		| Some idx ->
			let found = t.(idx) |> fst in
			let len = length t in
            if Key.compare key found = 0 then (
                let less, greater =
                    if idx = 0 then [||], Array.slice t 1 len 
                    else if idx = len-1 then Array.slice t 0 (len-1), [||]
                    else (
                        Array.slice t 0 idx, Array.slice t (idx+1) len
                    )
                in
                less, Some t.(idx), greater
            ) else (
                empty, None, t
            )
	;;

    let max_elt_exn t =
        t.(length t - 1)

    let find_exn t key =
        Option.value_exn (find t key)

    let remove (t : 'a t) key =
        match find_internal t ~kind:`First_equal_to key with
        | None -> t
        | Some key_idx ->
            let len = length t in
            let removed = Array.init (len - 1) ~f:(fun _ -> t.(0)) in
            let idx = ref 0 in
            for i=0 to len-1 do
                if i <> key_idx then (
                    removed.(!idx) <- t.(i);
                    incr idx
                )
            done;
            removed
    ;;

	let iter t ~f = Array.iter t ~f:(fun (_, value) -> f value)
	let fold t ~init ~f = 
		Array.fold t ~init ~f:(fun acc (key, data) ->
			f ~key ~data acc
		)

	let singleton key value = [|(key, value)|]

    let is_empty = Array.is_empty

	let for_all t ~f = Array.for_all t ~f:(fun (_, data) -> f data)

    let append ~lower_part:t1 ~upper_part:t2 =
        let len1 = length t1 in
        let len2 = length t2 in
        `Ok (Array.init (len1+len2) ~f:(fun idx ->
            if idx < len1 then (
                t1.(idx)
            ) else (
                t2.(idx - len1)
            )
        ))
    ;;

    let set (t : 'a t) ~(key : Key.t) ~(data : 'a) : 'a t =
        match find_internal t key with
        | None -> add_size_1 t (key, data)
        | Some idx ->
            let arr : 'a t = Array.copy t in
            arr.(idx) <- (key, data);
            Array.sort arr ~compare:(fun (key1, _) (key2, _) ->
				Key.compare key1 key2
			);
            arr
    ;;
end

module A = Make(Int)

let print a = 
    A.iter a ~f:(fun v -> Printf.printf "%s\n" v)

let%expect_test _ =
    let a = A.set A.empty ~key:0 ~data:"0" in
    let a = A.set a ~key:1 ~data:"1" in
    let x, _y, z = A.split a ~-1 in
    print a;
    Caml.print_endline "left";
    print x;
    Caml.print_endline "right";
    print z;
    [%expect {|
      0
      1
      left
      right
      0
      1 |}]

let insert t n =
    A.set t ~key:n ~data:Int.(to_string n)

let%expect_test _ =
    let a = insert A.empty 0 in
    let a = insert a 1 in
    let a = insert a 2 in
    let a = insert a 3 in
    let a = insert a 4 in
    let a = insert a 5 in
    let a = insert a 6 in
    let x, z = 
        match A.split a 5 with
        | x, None, y -> x, y
        | x, Some (k, v), y -> x, A.set y ~key:k ~data:v
    in
    print a;
    Caml.print_endline "left";
    print x;
    Caml.print_endline "right";
    print z;
  [%expect {|
    0
    1
    2
    3
    4
    5
    6
    left
    0
    1
    2
    3
    4
    right
    5
    6 |}]
