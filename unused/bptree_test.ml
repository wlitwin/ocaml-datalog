module BTree = Bptree.Make(Int)

let try_find tree key =
    try 
        match BTree.find tree key with
        | None -> Printf.printf "Key %d not found\n" key
        | Some v -> Printf.printf "Key %d Found %s\n" key v
    with e -> Caml.print_endline Exn.(to_string e)
;;

let conv = String.sexp_of_t
let conv_str = Fn.compose Sexp.to_string_hum String.sexp_of_t

let%expect_test _ =
    (* Add 10 elements *)
    let tree = BTree.create ~child_count:5 in
    let lcount = 10 in
    let arr = Array.init lcount ~f:(fun idx -> idx) in
    (*Array.permute arr;*)
    (*
    let rec loop count tree =
        if count <= 0 then tree
        else (
            loop (count-1) (BTree.insert tree count Int.(to_string count))
        )
    in
    let tree = loop lcount tree in
    *)
    let tree = Array.fold arr ~init:tree ~f:(fun tree idx -> 
        Printf.printf "Inserting %d\n" idx;
        BTree.insert tree idx Int.(to_string idx)
    ) in
    let count = BTree.fold tree ~init:0 ~f:(fun ~key:_ ~data:_ acc -> acc+1) in
    let count2 = ref 0 in
    BTree.iter2 tree ~f:(fun _ -> incr count2);
    Caml.print_endline BTree.(to_string tree conv);
    Printf.printf "Count should be %d = %d = %d" lcount count !count2;
    [%expect {|
      Inserting 0
      Inserting 1
      Inserting 2
      Inserting 3
      Inserting 4
      Inserting 5
      Inserting 6
      Inserting 7
      Inserting 8
      Inserting 9
      ((num_child_nodes 5)
       (root
        (Internal
         ((2 (Leaf ((0 0) (1 1) (2 2)))) (5 (Leaf ((3 3) (4 4) (5 5))))
          (9 (Leaf ((6 6) (7 7) (8 8) (9 9)))))))
       (leaves
        ((2 ((0 0) (1 1) (2 2))) (5 ((3 3) (4 4) (5 5)))
         (9 ((6 6) (7 7) (8 8) (9 9))))))
      Count should be 10 = 10 = 10 |}]
;;

let%expect_test _ = 
    (* Force leaf split *)
    let tree = BTree.create ~child_count:4 in
    let tree = BTree.insert tree 9 "9" in
    let tree = BTree.insert tree 3 "3" in
    let tree = BTree.insert tree 5 "5" in
    let tree = BTree.insert tree 1 "1" in
    let tree = BTree.insert tree 7 "7" in
    let tree = BTree.insert tree 6 "6" in
    let tree = BTree.insert tree 4 "4" in
    let tree = BTree.insert tree 8 "8" in
    let tree = BTree.insert tree 2 "2" in
    let tree = BTree.insert tree 10 "10" in
    Caml.print_endline BTree.(to_string tree conv);
    BTree.iter tree ~f:(fun value -> Printf.printf "%s " value);
    [%expect {|
      ((num_child_nodes 4)
       (root
        (Internal
         ((3 (Leaf ((1 1) (2 2) (3 3)))) (5 (Leaf ((4 4) (5 5))))
          (8 (Leaf ((6 6) (7 7) (8 8)))) (10 (Leaf ((9 9) (10 10)))))))
       (leaves
        ((3 ((1 1) (2 2) (3 3))) (5 ((4 4) (5 5))) (8 ((6 6) (7 7) (8 8)))
         (10 ((9 9) (10 10))))))
      1 2 3 4 5 6 7 8 9 10 |}]

let%expect_test _ =
    (* Check find *)
    let tree = BTree.create ~child_count:4 in
    let tree = BTree.insert tree 9 "9" in
    let tree = BTree.insert tree 3 "3" in
    let tree = BTree.insert tree 5 "5" in
    let tree = BTree.insert tree 1 "1" in
    let tree = BTree.insert tree 7 "7" in
    Caml.print_endline BTree.(to_string tree conv);
    try_find tree 10;
    try_find tree 2;
    try_find tree 9;
    try_find tree 3;
    try_find tree 5;
    try_find tree 1;
    try_find tree 7;
    [%expect {|
      ((num_child_nodes 4)
       (root (Internal ((5 (Leaf ((1 1) (3 3) (5 5)))) (9 (Leaf ((7 7) (9 9)))))))
       (leaves ((5 ((1 1) (3 3) (5 5))) (9 ((7 7) (9 9))))))
      Key 10 not found
      Key 2 not found
      Key 9 Found 9
      Key 3 Found 3
      Key 5 Found 5
      Key 1 Found 1
      Key 7 Found 7 |}]

let%expect_test _ =
    (* Check find after delete *)
    let tree = BTree.create ~child_count:4 in
    let tree = BTree.insert tree 9 "9" in
    let tree = BTree.insert tree 3 "3" in
    let tree = BTree.insert tree 5 "5" in
    let tree = BTree.insert tree 1 "1" in
    let tree = BTree.insert tree 7 "7" in
    Caml.print_endline BTree.(to_string tree conv);
    Caml.print_endline "Before 9 removed";
    let tree = BTree.remove tree 9 in
    Caml.print_endline "After 9 removed";
    let tree = BTree.remove tree 2 in
    Caml.print_endline "Before 5 removed";
    let tree = BTree.remove tree 5 in
    Caml.print_endline "After 5 removed";
    Caml.print_endline "Before 7 removed";
    let tree = BTree.remove tree 7 in
    Caml.print_endline "After 7 removed";
    Caml.print_endline BTree.(to_string tree conv);
    try_find tree 9;
    try_find tree 3;
    try_find tree 5;
    try_find tree 1;
    try_find tree 7;
    [%expect {|
      ((num_child_nodes 4)
       (root (Internal ((5 (Leaf ((1 1) (3 3) (5 5)))) (9 (Leaf ((7 7) (9 9)))))))
       (leaves ((5 ((1 1) (3 3) (5 5))) (9 ((7 7) (9 9))))))
      Before 9 removed
      After 9 removed
      Before 5 removed
      After 5 removed
      Before 7 removed
      After 7 removed
      ((num_child_nodes 4) (root (Internal ((3 (Leaf ((1 1) (3 3)))))))
       (leaves ((5 ((1 1) (3 3))) (9 ((7 7) (9 9))))))
      Key 9 not found
      Key 3 Found 3
      Key 5 not found
      Key 1 Found 1
      Key 7 not found |}]
;;

let%expect_test _ =
    let tree = BTree.create ~child_count:3 in
    let insert t n = BTree.insert t n Int.(to_string n) in
    (*let tree = insert tree 6 in*)
    let tree = insert tree 5 in
    let tree = insert tree 4 in
    let tree = insert tree 3 in
    let tree = insert tree 2 in
    let tree = insert tree 1 in
    (*
    let tree = BTree.insert tree 4 "4" in
    let tree = BTree.insert tree 7 "7" in
    let tree = BTree.insert tree 3 "3" in
    let tree = BTree.insert tree 1 "1" in
    let tree = BTree.insert tree 2 "2" in
    let tree = BTree.insert tree 9 "9" in
    let tree = BTree.insert tree 6 "6" in
    let tree = BTree.insert tree 10 "10" in
    let tree = BTree.insert tree 12 "12" in
    let tree = BTree.insert tree 15 "15" in
    let tree = BTree.insert tree 13 "13" in
    let tree = BTree.insert tree 17 "17" in
    let tree = BTree.insert tree 5 "5" in
    let tree = BTree.insert tree 8 "8" in
    let tree = BTree.insert tree 11 "11" in
    let tree = BTree.insert tree 0 "0" in
*)
    Caml.print_endline BTree.(to_string_hum tree conv_str);
    let tree = insert tree 6 in
    Caml.print_endline BTree.(to_string_hum tree conv_str);
    let tree = insert tree 7 in
    Caml.print_endline BTree.(to_string_hum tree conv_str);
    let tree = insert tree 8 in
    Caml.print_endline BTree.(to_string_hum tree conv_str);
    let tree = insert tree 9 in
    Caml.print_endline BTree.(to_string_hum tree conv_str);
    let tree = insert tree 10 in
    Caml.print_endline BTree.(to_string_hum tree conv_str);
    (*Caml.print_endline BTree.(to_string tree conv);*)
    [%expect {|
        3: [1:1 2:2 3:3 ]
        5: [4:4 5:5 ]
      3: [(1 1)(2 2)(3 3)]
      5: [(4 4)(5 5)]


        3: [1:1 2:2 3:3 ]
        6: [4:4 5:5 6:6 ]
      3: [(1 1)(2 2)(3 3)]
      6: [(4 4)(5 5)(6 6)]


        3: [1:1 2:2 3:3 ]
        5: [4:4 5:5 ]
        7: [6:6 7:7 ]
      3: [(1 1)(2 2)(3 3)]
      5: [(4 4)(5 5)]
      7: [(6 6)(7 7)]


        3: [1:1 2:2 3:3 ]
        5: [4:4 5:5 ]
        8: [6:6 7:7 8:8 ]
      3: [(1 1)(2 2)(3 3)]
      5: [(4 4)(5 5)]
      8: [(6 6)(7 7)(8 8)]


        5:
          3: [1:1 2:2 3:3 ]
          5: [4:4 5:5 ]
        9:
          7: [6:6 7:7 ]
          9: [8:8 9:9 ]
      3: [(1 1)(2 2)(3 3)]
      5: [(4 4)(5 5)]
      7: [(6 6)(7 7)]
      9: [(8 8)(9 9)]


        5:
          3: [1:1 2:2 3:3 ]
          5: [4:4 5:5 ]
        10:
          7: [6:6 7:7 ]
          10: [8:8 9:9 10:10 ]
      3: [(1 1)(2 2)(3 3)]
      5: [(4 4)(5 5)]
      7: [(6 6)(7 7)]
      10: [(8 8)(9 9)(10 10)] |}]
;;
