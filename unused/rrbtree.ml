(*
module Make(Value : sig type t val default : t end) = struct

    let rrb_bits = 5
    let rrb_branching = 1 lsl rrb_bits
    let rrb_mask = rrb_branching - 1
    let rrb_invariant = 1
    let rrb_extras = 2

    type leaf = {
        mutable len : int;
        mutable values : Value.t array; 
    }

    type size_table = int array

    type internal = {
        mutable len : int;
        mutable size_table : size_table;
        mutable children : node array;
    }

    and node = Leaf of leaf
             | Internal of internal

    type t = {
        mutable count : int;
        mutable shift : int;
        mutable tail : leaf;
        mutable tail_len : int;
        mutable root : node option;
    }

    let empty_leaf = {
        len = 0;
        values = [||];
    }

    let empty () = {
        count = 0;
        shift = 0;
        root = None;
        tail_len = 0;
        tail = empty_leaf;
    }

    let size_table_create size : size_table =
        Array.create ~len:size 0
    ;;

    let size_table_clone (orig : size_table) : size_table =
        Array.copy orig
    ;;

    let array_inc orig default =
        let len = Array.length orig in
        let new_arr = Array.create ~len:(len+1) default in
        Array.blito ~src:orig ~dst:new_arr ();
        new_arr
    ;;

    let array_dec orig =
        let len = Array.length orig in
        let new_arr = Array.create ~len:(len-1) orig.(0) in
        Array.blito ~src:orig ~dst:new_arr ~src_len:(len-1) (); 
        new_arr
    ;;

    let leaf_node_inc leaf_node =
        let values = array_inc leaf_node.values Value.default in
        { values; len = leaf_node.len+1 }
    ;;

    let leaf_node_dec leaf_node =
        let values = array_dec leaf_node.values in
        { values; len = leaf_node.len-1 }
    ;;

    let internal_node_inc node =
        let children = array_inc node.children (Leaf empty_leaf) in
        let size_table = array_inc node.size_table 0 in
        { children; size_table; len = node.len+1 }
    ;;

    let internal_node_dec node =
        let children = array_dec node.children in
        let size_table = array_dec node.size_table in
        { children; size_table; len = node.len-1 }
    ;;

    let create_leaf len = {
        len;
        values = Array.create ~len Value.default;
    }

    let clone_leaf leaf = {
        leaf with values = Array.copy leaf.values;
    }

    let rrb_head_clone t = {
        t with tail=clone_leaf t.tail
    }

	let rrb_shift n = n.shift

    let leaf_node_merge (left : leaf) (right : leaf) =
        let leaf = create_leaf (left.len + right.len) in
        Array.blito ~src:left.values ~dst:leaf.values ();
        Array.blito ~src:right.values ~dst:leaf.values ~dst_pos:left.len ();
        leaf
    ;;

    let create_internal len = {
        len;
        size_table = [||];
        children = Array.create ~len (Leaf empty_leaf);
    }

    let clone_internal internal = {
        internal with 
            size_table = Array.copy internal.size_table; 
            children = Array.copy internal.children;
    }

    let internal_node_copy orig start len =
        let copy = create_internal len in
        Array.blito ~dst:copy.children ~src:orig.children
                    ~src_pos:start ~src_len:len ();
        copy
    ;;

    let internal_node_new_above1 child =
        let above = create_internal 1 in
        above.children.(0) <- Internal child;
        above;
    ;;

    let internal_node_new_above left right =
        let above = create_internal 2 in
        above.children.(0) <- Internal left;
        above.children.(1) <- Internal right;
        above
    ;;

    let empty_internal () = {
        len = 0;
        size_table = [||];
        children = [||]
    }

    let internal_node_merge (left : internal) (center : internal) (right : internal) =
        let left_len = left.len - 1 in
        let center_len = center.len in
        let right_len = right.len - 1 in

        let merged = create_internal (left_len + center_len + right_len) in
        if left_len != 0 then (
            Array.blito ~src:left.children ~dst:merged.children ();
        );

        if center_len != 0 then (
            Array.blito ~src:center.children ~dst:merged.children ~dst_pos:left_len ();
        );

        if right_len != 0 then (
            Array.blito ~src:right.children ~dst:merged.children ~dst_pos:(left_len + center_len) ();
        );
        merged
    ;;

    let get_node_len = function
        | Leaf leaf -> leaf.len
        | Internal node -> node.len
    ;;

    let do_while ~f ~p =
        f();
        while p() do
            f()
        done;
    ;;

    let create_concat_plan (all : internal) =
        let node_count = Array.create ~len:all.len 0 in
        let total_nodes = ref 0 in
        for i=0 to all.len-1 do
            let size = all.children.(i) |> get_node_len in
            node_count.(i) <- size;
            total_nodes := !total_nodes + size;
        done;

        let optimal_slots = ((!total_nodes - 1)/ rrb_branching) + 1 in
        let shuffled_len = ref all.len in
        let i = ref 0 in
        while optimal_slots + rrb_extras < !shuffled_len do
            while node_count.(!i) > rrb_branching - rrb_invariant do
                incr i
            done;

            let remaining_nodes = ref node_count.(!i) in
            do_while ~p:(fun () -> !remaining_nodes > 0) ~f:(fun () ->
                let min_size = min (!remaining_nodes + node_count.(!i + 1)) rrb_branching in
                node_count.(!i) <- min_size;
                remaining_nodes := !remaining_nodes + node_count.(!i+1) - min_size;
                incr i
            );

            for j = !i to !shuffled_len-2 do
                node_count.(j) <- node_count.(j+1);
            done;

            decr shuffled_len;
            decr i;
        done;

        !shuffled_len, node_count
    ;;

	let dec_shift shift =
		shift - rrb_bits

	let inc_shift shift =	
		shift + rrb_bits

	let leaf_node_shift = 0

	let rec size_sub_trie node shift =
		match node with
		| Internal internal ->
			if Array.is_empty internal.size_table then (
				let len = internal.len in
				let child_shift = dec_shift shift in
				let last_size = size_sub_trie internal.children.(len - 1) child_shift in
				((len - 1) lsl shift) + last_size
			) else (
				internal.size_table.(internal.len - 1)
			)
		| Leaf leaf -> leaf.len
	;;

	let set_sizes (node : internal) shift =
		let sum = ref 0 in
		let table = size_table_create node.len in
		let child_shift = dec_shift shift in
		for i=0 to node.len-1 do
			sum := !sum + size_sub_trie node.children.(i) child_shift;
			table.(i) <- !sum;
		done;
		{ node with size_table=table }
	;;

    let rebalance left center right shift is_top =
        let all = internal_node_merge left center right in
        let top_len, node_count = create_concat_plan all in
        let new_all = execute_concat_plan all node_count top_len shift in
        if top_len <= rrb_branching then (
            if Bool.(is_top = false) then (
                internal_node_new_above1 (set_sizes new_all shift);
            ) else (
                new_all
            )
        ) else (
            let new_left = internal_node_copy new_all 0 rrb_branching in
            let new_right = internal_node_copy new_all rrb_branching (top_len - rrb_branching) in
            internal_node_new_above (set_sizes new_left shift) (set_sizes new_right shift)
        )
    ;;

    let rec find_shift = function
        | Leaf _ -> 0
        | Internal node ->
            rrb_bits + find_shift node.children.(0)
    ;;

	let value_exn = function
		| Some v -> v
		| None -> failwith "Expected some"

	let internal_exn = function
		| Internal node -> node
		| Leaf _ -> failwith "Expected internal"

	let rrb_tail_push rrb elt =
		let new_rrb = rrb_head_clone rrb in
		let new_tail = leaf_node_inc rrb.tail in
		new_tail.values.(new_rrb.tail_len) <- elt;
		{ new_rrb with count = rrb.count+1; tail_len=rrb.tail_len+1; tail=new_tail }
	;;

	let push_down_tail rrb new_rrb new_tail =
		let old_tail = new_rrb.tail in
		let new_rrb = {new_rrb with tail=new_tail} in
		if rrb.count <= rrb_branching then (
			{ new_rrb with 
				shift = leaf_node_shift; 
				root = Some (Leaf old_tail) 
			}
		) else (
			let index = ref (rrb.count - 1) in
			let nodes_to_copy = ref 0 in
			let nodes_visited = ref 0 in
			let pos = ref 0 in
			let current = ref (internal_exn (value_exn rrb.root)) in
			let shift = ref (rrb_shift rrb) in
			let exception Copyable_count_end in
			begin try
				while !shift > inc_shift leaf_node_shift do
					let child_index = ref 0 in
					if Array.is_empty !current.size_table then (
						let prev_shift = !shift + rrb_bits in
						if (!index lsr prev_shift) > 0 then (
							incr nodes_visited;
							raise Copyable_count_end;
						);
						child_index := (!index lsr !shift) land rrb_mask;
						index := !index land (lnot (rrb_mask lsl !shift));
						) else (
							child_index := !current.len - 1;
						  if !child_index <> 0 then (
							 index := !index - !current.size_table.(!child_index-1);
						  )
					  );
		incr nodes_visited;
		if !child_index < rrb_mask then (
			nodes_to_copy := !nodes_visited;
		  pos := !child_index;
						);

	current := !current.children.(!child_index) |> internal_exn;
	(*
	// This will only happen in a pvec subtree
	if (current == NULL) {
		nodes_to_copy = nodes_visited;
	  pos = child_index;

	  // if next element we're looking at is null, we can copy all above. Good
	  // times.
	  goto copyable_count_end;
			}
*)
	shift := !shift - rrb_bits;
		done;

			with Copyable_count_end -> ()
			end;
			
			while !shift > inc_shift leaf_node_shift do
				incr nodes_visited;	
				shift := !shift - rrb_bits;
			done;
			if !nodes_to_copy = 0 then (
				
			) else (
			);
			new_rrb
		);
	;;

	let rrb_push rrb elt =
		if rrb.tail_len < rrb_branching then (
			rrb_tail_push rrb elt
		) else (
			let new_rrb = clone_t rrb in
			let new_tail = create_leaf 1 in
			new_tail.values.(0) <- elt;
			let new_rrb = { new_rrb with tail=new_tail; tail_len=1; count = rrb.count+1} in
			push_down_tail rrb new_rrb new_tail
		)
	;;

    let concat (left : t) (right : t) : t =
        let concat_right_null () =
            let new_rrb = { (clone_t left) with count = left.count + right.count } in
            if left.tail_len = rrb_branching then (
                push_down_tail left new_rrb right.tail
            ) else if left.tail_len + right.tail_len <= rrb_branching then (
                let tail_len = left.tail_len + right.tail_len in
                let tail = leaf_node_merge left.tail right.tail in
                { new_rrb with tail; tail_len }
            ) else (
                let push_down = create_leaf rrb_branching in
                Array.blito ~dst:push_down.values ~src:left.tail.values ();
                let right_cut = rrb_branching - left.tail_len in
                Array.blito ~dst:push_down.values ~dst_pos:left.tail_len 
                            ~src:right.tail.values ~src_len:right_cut ();
                
                let new_tail_len = right.tail_len - right_cut in
                let new_tail = create_leaf new_tail_len in
                Array.blito ~dst:new_tail.values ~src:right.tail.values
                            ~src_pos:right_cut ~src_len:new_tail_len ();
                
                let new_rrb = { new_rrb with tail=push_down; tail_len=new_tail_len} in
                let left_imitation = { (clone_t left) with count = new_rrb.count - new_tail_len } in
                push_down_tail left_imitation new_rrb new_tail 
            )
        in

        let concat_right_non_null _root =
            let left = push_down_tail left (clone_t left) empty_leaf in
			let root_candidate = concat_sub_tree left.root (rrb_shift left) right.root (rrb_shift right) true in
			let new_shift = find_shift root_candidate in
			let new_rrb = { (clone_t empty) with count = left.count + right.count; shift=new_shift } in
			let root = set_sizes root_candidate (rrb_shift new_rrb) in
			{ new_rrb with root; tail=right.tail; tail_len=right.tail_len}
        in

        if left.count = 0 then right
        else if right.count = 0 then left
        else (
            match right.root with
            | None -> concat_right_null ()
            | Some root -> concat_right_non_null root
        )
    ;;
end
*)
