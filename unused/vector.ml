type 'a t = {
    count : int;
    values : 'a array;
}[@@deriving sexp]

let empty = {
    count = 0;
    values = [||]
}

let set (t : 'a t) idx value =
    let copy = Array.copy t.values in
    copy.(idx) <- value;
    { t with values = copy }
;;

let get t idx =
    t.values.(idx)

let iter t ~f =
    for i=0 to t.count-1 do
        f t.values.(i)
    done
;;

let fold t ~init ~f =
    let acc = ref init in
    for i=0 to t.count-1 do
        acc := f acc t.values.(i)
    done
;;

let append t value =
    let len = 
        let len = Array.length t.values in
        if t.count >= len then (
            Caml.float t.count *. 1.5 |> Int.of_float
        ) else len
    in
    let copy = Array.create ~len value in
    Array.blito ~src:t.values ~dst:copy (); 
    { count = t.count+1; values = copy }
;;

let length t = t.count

let remove t idx =
    let len = Array.length t.values in
    let shrink_cap = Int.of_float (Caml.float len*.0.75) in
    let copy =
        if t.count < shrink_cap then (
            (* Shrink this array *)
            Array.create ~len:shrink_cap t.values.(0)
        ) else (
            Array.copy t.values
        )
    in
    Array.blito ~src:t.values ~dst:copy ~src_len:(idx-1) ();
    Array.blit 
        ~src:t.values 
        ~dst:copy 
        ~len:(t.count - idx - 1)
        ~src_pos:(idx+1)
        ~dst_pos:idx;
    { count = t.count-1; values = copy }
;;

let calc_load len =
    Caml.float len *. 1.5 |> Int.of_float

let concat (t1 : 'a t) (t2 : 'a t) =
    if t1.count = 0 then t2
    else if t2.count = 0 then t1
    else (
        let size = t1.count + t2.count in
        let values = Array.create ~len:(calc_load size) t1.values.(0) in
        Array.blito ~src:t1.values ~dst:values ();
        Array.blito ~src:t2.values ~dst:values ~dst_pos:t1.count ();
        { count = size; values }
    )
;;

let insert t idx value =
    let result = append t value in
    (* Shift all the values down *)
    for i=t.count downto idx+1 do
        result.values.(i) <- result.values.(i-1)
    done;
    result.values.(idx) <- value;
    { count = t.count+1; values = result.values }
;;

let of_list lst =
    let values = Array.of_list lst in
    { count = Array.length values; values }
;;

let of_array arr =
    let values = Array.copy arr in
    { count = Array.length values; values }
;;

let print_array arr =
    iter arr ~f:(fun value -> Printf.printf "%d " value);
    Caml.print_endline ""

let%expect_test _ =
    let arr = of_list [1;2;3;4;5] in
    print_array arr;
    [%expect {| 1 2 3 4 5 |}]

let%expect_test _ =
    let arr = of_list [1;2;3;4;5] in
    let arr = remove arr 2 in 
    print_array arr;
    [%expect {| 1 2 4 5 |}]

let%expect_test _ = 
    let arr = of_list [1;2;3;4;5] in
    let arr = append arr 10 in
    print_array arr;
    [%expect {| 1 2 3 4 5 10 |}]

let%expect_test _ =
    let arr = of_list [1;2;3;4;5] in
    let arr = set arr 2 10 in
    print_array arr;
    [%expect {| 1 2 10 4 5 |}]

let%expect_test _ =
    let arr = of_list [1;2;3;4;5] in
    let arr = set arr 0 10 in
    let arr = remove arr 2 in
    print_array arr;
    [%expect {| 10 2 4 5 |}]

let%expect_test _ =
    let arr1 = of_list [1;2;3;4;5] in
    let arr2 = of_list [6;7;8;9;10] in
    print_array (concat arr1 arr2);
    [%expect {| 1 2 3 4 5 6 7 8 9 10 |}]

let%expect_test _ =
    let arr = of_list [1;2;3;4;5] in
    let arr = insert arr 2 10 in
    print_array arr;
    let arr = insert arr 6 20 in
    print_array arr;
    let arr = insert arr 0 40 in
    print_array arr;
    [%expect {|
      1 2 10 3 4 5
      1 2 10 3 4 5 20
      40 1 2 10 3 4 5 20 |}]

let%expect_test _ =
    let arr = of_list [1;2;3;4;5] in
    Printf.printf "%d " (get arr 0);
    Printf.printf "%d " (get arr 1);
    Printf.printf "%d " (get arr 2);
    Printf.printf "%d " (get arr 3);
    Printf.printf "%d " (get arr 4);
    [%expect {| 1 2 3 4 5 |}]
