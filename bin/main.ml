let _ =
    ()
    (*
    let open Ocaml_datalog.Datomic2 in
    run();
    *)
    (*
    let store = Store.load_or_create "/home/walter/fs_store/" in
    begin match store with
    | Error `Failed_to_write_db -> Caml.print_endline "Error writing store"
    | Error `Invalid_directory -> Caml.print_endline "Specified directory is invalid"
    (*| Error `Invalid_db_file -> Caml.print_endline "Invalid DB File"
    | Error `Invalid_directory -> Caml.print_endline "Invalid directory"
    | Error `Invalid_directory_structure -> Caml.print_endline "Invalid directory structure"
    *)
    | Ok store -> 
        Store.add_file store "/home/walter/cubes.mp4" |> ignore;
        Caml.print_endline Ocaml_datalog.Db.(to_string store.db);
    end;
    *)
    (*Ocaml_datalog.Repl.repl()*)
    (*
    Ocaml_datalog.Util.timeit "full run" (fun () ->
        Ocaml_datalog.Examples.run()
    )
    *)
    (*Filestore.Repl.repl()*)
    (*
    let open Ocaml_datalog in
    let open Db in
    let open EntityDesc in
    let ed = use Db.empty in
    let id = make ed in
    let d = datom id "cool" (Edn (`String "cool")) in
    add ed d;
    let db = Db.add ed in
    Db.write_to_file_binary db "db.db";
    let db = 
        match Db.read_from_file_binary "db.db" with
        | Ok db -> db
        | Error e -> 
            Caml.print_endline "Errored"; Db.empty
    in
    Db.to_string db |> Caml.print_endline
    *)
